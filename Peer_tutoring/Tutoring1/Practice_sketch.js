function setup() {
  createCanvas(windowWidth,windowHeight);
  frameRate(30);
  angleMode(DEGREES);
}

var rot =0;
function draw() {
  background(200,200,200);



  push();
  translate(150,200);
  strokeWeight(10);
  fill(200,0,100);
  rect(0,0,50,50);

    push();
    strokeWeight(5);
    fill(100,200,100);
    rotate(rot + 90)
    rect(0,0,50,50);
    pop();

    push();
    strokeWeight(10);
    fill(0,300,100);
    rotate(rot)
    rect(0,0,50,50);
    pop();

  rot += 1;
  pop();


  translate(windowWidth/2, windowHeight/2);
  rect(0,0,50,50);

}
