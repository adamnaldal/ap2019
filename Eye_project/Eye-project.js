let rotate = 0.393;
var x = 300;
var y = 250;
var easing = 0.15;
var lidH = 0;
var a = 0.4;
var s = 0.0;

function setup() {
  createCanvas(600,500);
  frameRate(30);
  noStroke();
}

function draw() {
  background(100);

  //Draws eyeball
  angleMode(RADIANS);
  let c = color(400);
  fill(c);
  arc(width/2, height/2+19.5, 100,100, PI + QUARTER_PI-rotate, TWO_PI-rotate,OPEN);
  arc(width/2, height/2-19.5, 100,100, PI + PI + QUARTER_PI-rotate, PI+TWO_PI-rotate,OPEN);

  //Constrains pupil
  var targetX = constrain(mouseX, 275, 325);
  var dx = targetX - x;
  x += dx * easing;
  var targetY = constrain(mouseY,235, 265);
  var dy = targetY - y;
  y += dy * easing;

  //Draws pupil
  c = color(10);
  fill(c);
  ellipse(x, y, 50,50);

  //eyelids
  fill(c);
  ellipse(width/2, height/2-35,200,lidH);
  ellipse(width/2, height/2+35,200,lidH);

  //Increments the blinking control numbers
  a = a + 0.04;
  s = cos(a);

  //calls the blink function occasionally
  blink();
}

//blinks when mouse is pressed within an area rougle corresponding to the eye, and resets the time until the next blink
function mousePressed(){
  if(mouseX < 340){
    if(mouseX > 260){
      if(mouseY < 278){
        if(mouseY > 225){
          a = 6.14;
          blink();
        }
      }
    }
  }
}

//defines the blink function, the eyelids will close, the pupil will reset to the center of the eye and the eyelids will open again
function blink(){
  if (s > 0.99){
    lidH = lidH + 60;
    if (lidH > 80){
      x = 300;
      y = 250;
    }
  }
  else{
    lidH = lidH / 4;
  }
}
