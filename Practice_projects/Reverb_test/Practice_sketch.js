var soundFile, reverb;
function preload() {
  soundFile = loadSound('Damscray_Dancing_Tiger.mp3');
}

function setup() {
  reverb = new p5.Reverb();
  // so we'll only hear reverb...

  // connect soundFile to reverb, process w/
  // 3 second reverbTime, decayRate of 2%
  reverb.process(soundFile, 10, 1);
  soundFile.play();
}
