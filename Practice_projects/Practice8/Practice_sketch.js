let innerR;
let outerR;
let i = 0;
let startPoints = [];
let endPoints = [];
let curves = [];
let spacing = 4;
let colorOfset;
let done = false;
let saved = false;
let randomStart;

function setup() {
  colorMode(HSB, 360, 100, 100, 100);
  angleMode(DEGREES);
  noFill();
  frameRate();
  createCanvas(800, 800);
  noiseDetail(8, 0.24)

  colorOfset = random(360);
  innerR = (width / 16);
  outerR = (width / 208) * 100;
  randomStart = random(360);

}


function draw() {
  background(5);

  translate(width / 2, height / 2);


  if (startPoints.length <= 360 / spacing) {
    //generates points on the inner circle
    let innerX = innerR * sin(randomStart + i * spacing);
    let innerY = innerR * cos(randomStart + i * spacing);
    let innerVector = createVector(innerX, innerY);
    startPoints.push(innerVector)

    //generates points on outer cicle
    let randomOuter = map(noise(i / 40), 0, 1, 0, 520) + randomStart;
    let outerX = outerR * sin(randomOuter);
    let outerY = outerR * cos(randomOuter);
    let outerVector = createVector(outerX, outerY);
    endPoints.push(outerVector);

    //generates bezier curves
    let baseAngle = startPoints[i].angleBetween(endPoints[i])
    let a1 = baseAngle + map(noise(i / 0.2), 0, 1, -0.02, 0.02);
    let a2 = baseAngle + map(noise(i / 0.2 + 700), 0, 1, -0.02, 0.02);

    let r1 = dist(startPoints[i].x, startPoints[i].y, endPoints[i].x, endPoints[i].y) * 0.3;
    let r2 = dist(startPoints[i].x, startPoints[i].y, endPoints[i].x, endPoints[i].y) * 0.6;

    if (a1 < 90) {
      var anchor1x = startPoints[i].x - r1 * sin(a1);
      var anchor1y = startPoints[i].y - r1 * cos(a1);
    } else {
      var anchor1x = startPoints[i].x + r1 * sin(a1);
      var anchor1y = startPoints[i].y + r1 * cos(a1);
    }

    if (a2 < 90) {
      var anchor2x = startPoints[i].x - r2 * sin(a2);
      var anchor2y = startPoints[i].y - r2 * cos(a2);
    } else {
      var anchor2x = startPoints[i].x + r2 * sin(a2);
      var anchor2y = startPoints[i].y + r2 * cos(a2);
    }

    // let ancorPoint1 = createVector(anchor1x, anchor1y);
    // let ancorPoint2 = createVector(anchor2x, anchor2y);

    curves.push([startPoints[i].x, startPoints[i].y, anchor1x, anchor1y, anchor2x, anchor2y, endPoints[i].x, endPoints[i].y])

    i++
  }

  for (let j = 0; j < startPoints.length - 1; j++) {
    if (j + colorOfset <= 360) {
      stroke(j + colorOfset, 90, 90)
    } else {
      stroke(j + colorOfset - 360, 90, 90)
    }
    bezier(curves[j][0], curves[j][1], curves[j][2], curves[j][3], curves[j][4], curves[j][5], curves[j][6], curves[j][7]);
    if (j == 360 / spacing - 1) {
      done = true;
    }
  }

  if (done && !saved) {
    saveCanvas('hoop', 'png')
    saved = true;
  }
}