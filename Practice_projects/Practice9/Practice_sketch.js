let strokes = [];
let widthU;
let heightU;

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(60);
  colorMode(HSB, 360, 100, 100, 100)
  angleMode(DEGREES);

  widthU = floor(width / 25);
  heightU = floor(height / 25);
}

function draw() {

  background(260, 90, 90);
  stroke(260, 5, 100);
  strokeWeight(3)
  noFill()

  for (let i = 0; i < 3; i++) {
    if (strokes.length < (widthU * heightU)) {
      let type = floor(random(4));

      if (type == 0) {
        strokes.push([-25, 0, 50, 50, -90, 0])
      } else if (type == 1) {
        strokes.push([-25, -25, 50, 50, 0, 90])
      } else if (type == 2) {
        strokes.push([0, -25, 50, 50, 90, 180])
      } else if (type == 3) {
        strokes.push([0, 0, 50, 50, 180, 270])
      }
    }
  }
  translate(25, 25)
  for (let i = 0; i < strokes.length; i++) {
    let y = floor(i / widthU) * 25;
    let x = floor(i % widthU) * 25;

    push()
    translate(x, y);
    arc(strokes[i][0], strokes[i][1], strokes[i][2], strokes[i][3], strokes[i][4], strokes[i][5])
    pop()
  }
}