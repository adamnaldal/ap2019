let w = 600;
let h = 500;

//variables related to positioning and moving the pupil
let pupilX = w / 2;
let pupilY = h / 2;
let easing = 0.2;
let v1;
let pupilHeading;
let diameterx;
let diametery;
let xMax;
let yMax;
let targetX;
let targetY;

function setup() {
  createCanvas(w, h);
  frameRate(30);
}

function draw() {
  angleMode(DEGREES);
  background(100); //purple
  //let iy = map(sin(frameCount), -1, 1, 0, 40);
  let iy = 40;

  push();
  // figures out the angle from the center of the eye to the mouse position
  v1 = createVector(mouseX - width / 2, mouseY - height / 2)
  pupilHeading = v1.heading()
  //defines the area that the pupil can move within as an ellipse using some trigonometry
  diameterx = 35;
  diametery = 23;
  xMax = diameterx * cos(pupilHeading);
  yMax = diametery * sin(pupilHeading);

  //Constrains pupil to the ellipse defined above
  if (dist(width / 2, height / 2, mouseX, mouseY) >= dist(width / 2, height / 2, width / 2 + xMax, height / 2 + yMax)) {
    targetX = width / 2 + xMax;
    targetY = height / 2 + yMax;
  } else {
    targetX = mouseX;
    targetY = mouseY;
  }
  let dx = targetX - pupilX;
  pupilX += dx * easing;
  let dy = targetY - pupilY;
  pupilY += dy * easing;
  fill(10);
  ellipse(pupilX, pupilY, 50, 50);
  pop();

  ///ydre øje
  push();
  beginShape();
  translate(width / 2, height / 2);
  fill(200, 200, 0);
  beginShape();
  for (let i = 0; i < 360; i++) {
    let x = 60 * cos(i);
    let y = 55 * sin(i);
    vertex(x, y);
  }
  beginContour();
  vertex(+45, 0);
  bezierVertex(+27, 0 - iy, -27, 0 - iy, -45, 0);
  bezierVertex(-27, 0 + iy, +27, 0 + iy, +45, 0);
  endContour();
  endShape();
  pop();

}

function mousePressed() {
  console.log(mouseX, mouseY);
}