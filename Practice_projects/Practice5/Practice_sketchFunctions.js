let currentRoom;
let east;
let west;
let rooms = [];

function setup() {
  createCanvas(500, 400);
  newRoom("the kitchen", "The rooms is warm. Sunlight filters in trough the window and lands on the cutting board", "east", " north");
  newRoom("The living room", "The sofa is placed in front of the tv.", "south", " north", "west")
  // east = createButton('east');
  // north = createButton('north');
  // south = createButton('south');
  // west = createButton('east');
  currentRoom = rooms[0];
}


function draw() {
  background(100);
  displayRoom();

  noLoop();
}

function newRoom(_title, _description, _dir1, _dir2, _dir3, _dir4, _dir5, _dir6) {
  this.title = _title;
  this.description = _description;
  this.dir1 = _dir1;
  this.dir2 = _dir2;
  this.dir3 = _dir3;
  this.dir4 = _dir4;
  this.dir5 = _dir5;
  this.dir6 = _dir6;
  this.buttons = [];


  let protoDirections = [this.dir1, this.dir2, this.dir3, this.dir4, this.dir5, this.dir6]
  let dirs = protoDirections.filter(word => word);
  console.log(dirs);
  this.dirs = dirs;


  for (let i = 0; i < this.dirs.length; i++) {
    button = createButton(this.dirs[i]);
    button.position(20 + i * 50, 360)
    button.hide();
    this.buttons.push(button);
    console.log(button)
  }

  rooms.push([this.title, this.description, [this.dirs],
    [this.buttons]
  ]);

}

function displayRoom() {
  push();
  textAlign(CENTER, BOTTOM);
  textSize(18);
  text("You are in " + currentRoom[0], width / 2, height / 2);

  push();
  textSize(12);
  text(currentRoom[1], width / 2, height / 2 + 20);
  text("You can go " + currentRoom[2], width / 2, height / 2 + 40)
  pop();

  pop();

  for (let elt of currentRoom[3]) {
    console.log(elt);
    let button = elt;
  }

}