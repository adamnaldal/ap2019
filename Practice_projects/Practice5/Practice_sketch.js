let currentRoom;
let rooms = [];
let change;

let kitchen = {
  title: "",
  description: "",
  items: [],
  exits: [],
  buttons: []
};

function flip() {
  console.log("flip")

  if (currentRoom == rooms[0]) {
    currentRoom = rooms[1];
    console.log("1")
  } else {
    currentRoom = rooms[0];
    console.log("0")
  }
  gotoRoom();
}

function gotoRoom() {
  let allButtons = selectAll('button')
  console.log(allButtons);
  for (let bs of allButtons) {
    bs.hide();
  }
}

function setup() {
  frameRate(30);
  createCanvas(500, 400);
  newRoom("the kitchen", "The rooms is warm. Sunlight filters in trough the window and lands on the cutting board", "east", " north");
  newRoom("The living room", "The sofa is placed in front of the tv.", "south", " north", " west")
  currentRoom = rooms[0];
  change = createButton("change");
  change.position(20, 20);
  change.mousePressed(flip);
}


function draw() {
  background(100);
  displayRoom();
  //console.log(currentRoom.buttons[0]);
  change.show();
}

function newRoom(_title, _description, _dir1, _dir2, _dir3, _dir4, _dir5, _dir6) {
  this.title = _title;
  this.description = _description;
  this.dir1 = _dir1;
  this.dir2 = _dir2;
  this.dir3 = _dir3;
  this.dir4 = _dir4;
  this.dir5 = _dir5;
  this.dir6 = _dir6;

  let protoDirections = [this.dir1, this.dir2, this.dir3, this.dir4, this.dir5, this.dir6]
  let dirs = protoDirections.filter(word => word);

  let buttons = [];
  for (let exit = 0; exit < dirs.length; exit++) {
    button = createButton(dirs[exit])
    buttons.push(button);
    buttons[exit].hide()
  }

  let room = {
    title: this.title,
    description: this.description,
    items: [],
    exits: dirs,
    buttons: buttons
  };

  rooms.push(room);
}

function displayRoom() {
  push();
  textAlign(CENTER, BOTTOM);
  textSize(18);
  text("You are in " + currentRoom.title, width / 2, height / 2);

  push();
  textSize(12);
  text(currentRoom.description, width / 2, height / 2 + 20);
  text("You can go " + currentRoom.exits, width / 2, height / 2 + 40)
  pop();

  pop();
  for (let i = 0; i < currentRoom.buttons.length; i++) {
    currentRoom.buttons[i].show();
  }
}