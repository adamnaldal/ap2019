let tempValue = 420;
let soundValue = 69;

let client;
let key = "counterSmartHome";
let secret = "getThatEvidence";
let id = "fakeKey"

let weird = false;
let oldWeird = false;

let currentTime = 0;

let responsePending = false;

let entry;
let log = [];

let img;

function preload(){
  img = loadImage("key.png");
}

function setup() {
  createCanvas(600,300);

  // MQTT TING START
  // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
  client = mqtt.connect('mqtt://' + key + ':' + secret + '@broker.shiftr.io', {
    id: 'fakeKey'
  });

  client.on('connect', function() {
    console.log('client has connected!');


    // HER SUBCRIBER VI TIL DE ADRESSER VI VIL LYTTE TIL
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    client.subscribe('/tempTooKey');
    client.subscribe('/soundTooKey');
    // client.unsubscribe('/example');
  });

  // HER FORTÆLLER VI AT VI VIL MODTAGE BESKEDERNE I VORES FUNKTION: messageReceived()
  // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
  client.on('message', function(topic, message) {
    messageReceived(topic, message);
  });

  // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
  // MQTT TING SLUT

}

function draw(){
  background(255);
  image(img, 180,50,200,200);

  text("LAST EVENT:", 420,50);
  if (log.length >0){
  text("time: " + log[log.length-1].time, 420, 70);
    text("volume: " + log[log.length-1].sound,420, 90);
    text("Temperature: " + log[log.length-1].temp,420,110);
    text("Uncomfortable?: " + log[log.length-1].weird,420,130)
  }

  currentTime = millis();
  // fill(0);
   text("current state: " + weird,23,30)
  // text("log length: " + log.length,23,50)
  // for (let i = 0; i < log.length; i ++){
  //   text("weirdness of event " + i + ": " + log[i].weird ,23,(65 + i * 15));
  // }
}

//the class for log entry objects
class logEntry {
  constructor(_weirdo,_sound,_temp){
    this.time = (day()+ "/" + month() + "/" + year() + " – " + hour() + ":" + minute() + ":" + second());
    this.temp = _temp;
    this.sound = _sound;
    this.weird = _weirdo;
    this.counter = currentTime;
  }
}

function mousePressed(){
  if (mouseX < 320 + 65 && mouseX > 320 - 65 && mouseY > 110 - 65 && mouseY < 110 + 65){
    buttonPressed();
  }
}

function buttonPressed(){

  weird = true;
  oldWeird = true;
  responsePending = true;

  if (log.length > 0){
    if (currentTime - log[log.length-1].counter > 5000){
      publishMessage("/keyTooSound",weird + ",");
    } else {
      log[log.length-1].weird = true;
    }
  } else {
    publishMessage("/keyTooSound",weird + ",");
  }
  setTimeout(function unWeird(){weird = false;},5000);
}

// DEN HER FUNKTION SKYDER HVER GANG VI MODTAGER EN BESKED
function messageReceived(t, m){
  var topic = t.toString();
  var message = m.toString();

  if (responsePending){
    let array = split(message,',');

    let sound = array[0];
    let temp = array[1];

    let entry = new logEntry(oldWeird,sound,temp);
    log.push(entry);

    responsePending = false;
    oldWeird = false;

  } else if(responsePending == false){
    let array = split(message,',');

    let sound = array[0];
    let temp = array[1];

    let entry = new logEntry(weird,sound,temp);
    log.push(entry);
  }

  console.log(log);
}

// DEN HER FUNKTION KALDER VI NÅR VI VIL SENDE EN BESKED
function publishMessage(topic, message){
    client.publish(String(topic), String(message));
}
