let resultings;

function preload() {
  jQuery.ajax({
    url: 'https://getbible.net/json?',
    dataType: 'jsonp',
    data: 'passage=Luke',
    jsonp: 'getbible',
    success: function(json) {
      // set text direction
      if (json.direction == 'RTL') {
        var direction = 'rtl';
      } else {
        var direction = 'ltr';
      }
      // check response type
      if (json.type == 'verse') {
        resultings = json;
      } else if (json.type == 'chapter') {
        resultings = json;
      } else if (json.type == 'book') {
        resultings = json;
      }
    },
    error: function() {
      console.log("ERROR : no scripture was returned")
    },
  });
}

function setup() {
  createCanvas(600, 600)
}

function draw() {
  background(100)

}