let testButton;
let playlists;
let buttons = [];
let moods = []; //moods are just classes
let checkBoxes =[];


function preload(){
  playlists = loadJSON("playlists.json");
}

function setup() {
  noCanvas();


  /*########################
  ###   Create Buttons   ###
  ########################*/
  //Creates the buttons from the json list of playlists and stores them in the array buttons
  for(playlist of playlists.list){
    buttons.push(createButton(playlist.label));
  }


  /*########################
  ###  Process Buttons   ###
  ########################*/
  for(let i = 0; i<buttons.length; i++){

    /*########################
    ###    Assign Links    ###
    ########################*/
    //attaches the corresponding link to the button
    buttons[i].mousePressed(function(){window.open(playlists.list[i].link,"_top");})

    if (playlists.list[i].mood !== undefined){
      buttons[i].class(playlists.list[i].mood);
    } else{
      buttons[i].class("basic");
    }

    /*########################
    ###     Add Moods      ###
    ########################*/
    if (moods.includes(playlists.list[i].mood) == false){
      moods.push(buttons[i].class());
    }

    //put buttons in button box flex container
    buttons[i].parent("buttonBox");
  }

  /*########################
  ###    add selectors   ###
  ########################*/
  for (each of moods){
    checkBoxes.push(createButton(each, true));
  }

  //organise selectors under selector box flex container
  console.log(checkBoxes.length)
  for (let i = 0; i < checkBoxes.length; i++){
    checkBoxes[i].id(moods[i]+"Selector");
    checkBoxes[i].attribute("name",moods[i]);
    checkBoxes[i].attribute("onclick","updateVisibility(this.name)");
    checkBoxes[i].class("filterDiv");
    checkBoxes[i].parent("selectorBox");
    console.log("run " + i);
  }
  updateVisibility("old_plot");

  ;
}

 function updateVisibility(subject){
   let subjects = selectAll("." + subject);
   console.log(subjects[0].elt.hidden)
   if (subjects[0].elt.hidden){
     //show playlist buttons
     for (each of subjects){
       each.show();
       each.elt.hidden = false;
     }

     //reset selector class
     let selectorTooChange = select('#' + subject + 'Selector');
     selectorTooChange.class("filterDiv");
   } else {
     //hide playlist buttons
     for (each of subjects){
       each.hide();
       each.elt.hidden = true;
     }

     //change selector class to deselected
     let selectorTooChange = select('#' + subject + 'Selector');
     selectorTooChange.class("deselected");
   }
 }
