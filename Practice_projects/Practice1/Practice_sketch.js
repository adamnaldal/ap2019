function setup() {
  createCanvas(600,500);
  frameRate();
}

var r = 10;

function draw() {
  angleMode(DEGREES);
  background(100,30,400);
  rectMode(CENTER);
  noStroke();

  fill(50,255,130);
  translate(mouseX,mouseY);

  //Rotate the whole plus slowly and varies speed by mouse movement
  r += - 0.5 + (mouseX - pmouseX + mouseY - pmouseY) * 1.2;
  rotate(r);

  //Draw the whole plus
  rect(0,0,20,80,40);
  rotate(90);
  rect(0,0,20,80,40);
}
