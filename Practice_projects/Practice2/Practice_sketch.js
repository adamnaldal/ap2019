
function setup() {
  createCanvas(600, 500, WEBGL);
  angleMode(DEGREES);
  frameRate(30);
}

function draw() {
  background(10);
  noStroke();

  pointLight(250, 50, 200, -200, -100, cos(frameCount)* 400);
  pointLight(100, 30, 250, cos(frameCount)*500 ,100, cos(frameCount)*-800);
  pointLight(150, -200,cos(frameCount)*  -100, cos(frameCount)* -200);
  ambientMaterial(250);

  push();
  var posC = cos(frameCount);
  translate(0,posC*120);
  scale(posC*1.6);
  sphere(25);
  pop();

  rotateX(90);
  rotateY(cos(frameCount)*4);
  rotateX(cos(frameCount*1.4)*8);
  rotateZ(frameCount*0.8);
  torus(120,15, 16, 16);


}
