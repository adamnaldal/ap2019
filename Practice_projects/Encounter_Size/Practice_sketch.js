let cr = 5;
let monsters = [];

function preload() {
  expAwarded = loadJSON("expAwarded.json")
}

function setup() {
  createCanvas(200, 200)
  monsters.push(new foe("0"))
  console.log(monsters[0])
}

class foe {
  constructor(cr) {
    this.cr = cr;
    this.exp;
    this.expSelector();
  }

  expSelector() {
    if (this.cr == "0") {
      this.exp = 10;
    } else if (this.cr == "1/8") {
      this.exp = 25;
    } else if (this.cr == "1/4") {
      this.exp = 50;
    } else if (this.cr == "1/2") {
      this.exp = 100;
    } else if (this.cr == "1") {
      this.exp = 200;
    } else if (this.cr == "2") {
      this.exp = 450;
    } else if (this.cr == "3") {
      this.exp = 700;
    } else if (this.cr == "4") {
      this.exp = 1100;
    } else if (this.cr == "5") {
      this.exp = 1800;
    } else if (this.cr == "6") {
      this.exp = 2300;
    } else if (this.cr == "7") {
      this.exp = 2900;
    } else if (this.cr == "8") {
      this.exp = 3900;
    } else if (this.cr == "9") {
      this.exp = 5000;
    } else if (this.cr == "10") {
      this.exp = 5900;
    } else if (this.cr == "11") {
      this.exp = 7200;
    } else if (this.cr == "12") {
      this.exp = 8400;
    } else if (this.cr == "13") {
      this.exp = 10000;
    } else if (this.cr == "14") {
      this.exp = 11500;
    } else if (this.cr == "15") {
      this.exp = 13000;
    } else if (this.cr == "16") {
      this.exp = 15000;
    } else if (this.cr == "17") {
      this.exp = 18000;
    } else if (this.cr == "18") {
      this.exp = 20000;
    } else if (this.cr == "19") {
      this.exp = 22000;
    } else if (this.cr == "20") {
      this.exp = 25000;
    } else if (this.cr == "21") {
      this.exp = 33000;
    } else if (this.cr == "22") {
      this.exp = 41000;
    } else if (this.cr == "23") {
      this.exp = 50000;
    } else if (this.cr == "24") {
      this.exp = 62000
    } else if (this.cr == "25") {
      this.exp = 75000
    } else if (this.cr == "26") {
      this.exp = 90000
    } else if (this.cr == "27") {
      this.exp = 105000
    } else if (this.cr == "28") {
      this.exp = 120000
    } else if (this.cr == "29") {
      this.exp = 135000
    } else if (this.cr == "30") {
      this.exp = 155000
    } else {
      this.exp = "Invalid CR"
    }
  }

}