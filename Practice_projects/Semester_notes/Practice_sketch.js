//this script accepts json files parsed from https://ical-to-json.herokuapp.com/
//texts for the presentation should be put in description and sepperated by "\\n" (literally: backslash,backslash,n)
let jsonFile;
let makerButton;
let clearButton;
let filePicker;
let infoText;
let semester = 4;
let created;
let bookmark = [-1,17,18,20,21,22,24,27,29,31,32,34,35,36,37,38,5,42,43,0,1,3,6,7,8,9,12];
let testModeCheck;

function preload() {
  jsonFile = loadJSON('4_semester.json');
}

function setup() {
  noCanvas();
  infoText = createDiv("<p>This program accepts json files created with <a href='https://ical-to-json.herokuapp.com/' target='_blank'>ical-to-json.herokuapp.com</a>.</p><p>Once a json file has been made it should be modified to include the texts for the course in their corresponding 'description' objects.</p>");
  makerButton = createButton("Make Bear Notes");
  makerButton.mouseClicked(function() {
    if (millis() > 1) {
      makeNotes();
    }
  });
//NOTE: for some reason the function in mouseClicked fires at startup, making te button redundant. therefore i added a 1 millisecond period where the button does nothing, preventing this unwanted press.
  clearButton = createButton("Clear index of created notes from storage.");
  clearButton.mouseClicked(function() {
    if (millis() > 1) {
      created = [-1];
    }
  });

  if (localStorage.getItem("notesIndex").length > 0){
    created = JSON.parse(localStorage.getItem("notesIndex"));
  } else {
    created = [-1];
  }

  console.log(localStorage.getItem("notesIndex"));

  testModeCheck = createCheckbox("Test Mode",false);

}


function makeNotes() {
  for (k = 0; k < jsonFile.vcalendar[0].vevent.length; k++) {
    let each = jsonFile.vcalendar[0].vevent[k];

    if (each.description !== "") {

      if (created.some(function doublesFilter(index) {
          return index == k;
        }) == false) {

          console.log(k);
        //date parser
        let rawDate = each.dtstart[0];
        let monthNr = rawDate.substring(4, 6)
        let dayNr = rawDate.substring(6, 8)
        let month;
        let alfabetize = function() {
          if (monthNr == '01') {
            month = "Jan"
          } else if (monthNr == '02') {
            month = "Feb"
          } else if (monthNr == '03') {
            month = "Mar"
          } else if (monthNr == '04') {
            month = "Apr"
          } else if (monthNr == '05') {
            month = "Maj"
          } else if (monthNr == '06') {
            month = "Jun"
          } else if (monthNr == '07') {
            month = "Jul"
          } else if (monthNr == '08') {
            month = "Aug"
          } else if (monthNr == '09') {
            month = "Sep"
          } else if (monthNr == '10') {
            month = "Okt"
          } else if (monthNr == '11') {
            month = "Nov"
          } else if (monthNr == '12') {
            month = "Dec"
          } else {
            month = monthNr
          }
        }
        alfabetize();
        //END date parser

        //Text creator
        let title = dayNr + " " + month + " – ";
        let body;
        let tag;

        let texts = split(each.description, "\\n");

        //title composer
        for (let i = 0; i < texts.length; i++) {
          title = title + texts[i]
          if (i !== texts.length - 1) {
            title = title + ", "
          }
        }
        let encodedTitle = encodeURIComponent(title);
        //END title composer

        //index maker
        let index = "–––––––––––––[FOLDER](file:/Users/adamnaldal/Desktop/Uni/" + semester + "_semester/" + each.summary + ")–––––––––––––\n\n";
        for (let i = 0; i < texts.length; i++) {
          index = index + (i + 1) + ". [Tekstnoter – " + texts[i] + "](bear://x-callback-url/open-note?title=" + encodedTitle + "&header=Tekstnoter%20%E2%80%93%20" + encodeURIComponent(texts[i]) + ")\n";
          index = index + "\t* [Tekst " + (i + 1) + "](bear://x-callback-url/open-note?title=" + encodedTitle + "&header=Tekst%20" + (i + 1) + ")\n";
        } //END index maker

        //body maker
        body = index + "---\n";
        for (let i = 0; i < texts.length; i++) {
          body = body + "\n## Tekstnoter – " + texts[i] + "\n* \n\n### Tekst " + (i + 1) + "\n\n---\n";
        }
        let encodedBody = encodeURIComponent(body);
        //END body maker

        //tag maker
        tag = "uni/" + semester + "/" + each.summary;
        let encodedTag = encodeURIComponent(tag);
        //END tag maker

        //send to bear
        window.open("bear://x-callback-url/create?&show_window=no&title=" + encodedTitle + "&text=" + encodedBody + "&tags=" + encodedTag)

        created.push(k);
      }
    }
  }

  if (testModeCheck.checked() === false){
    localStorage.setItem("notesIndex", JSON.stringify(created));
  }

} //END Bear note maker
