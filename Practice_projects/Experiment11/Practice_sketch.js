/*
node ~/node_modules/p5.serialserver/startserver.js
*/

let konkylie;
let volume;
let panning;
let leftVal = 0;
let rightVal = 0;
let volAdjustment = 0;
let startSum = 988;

var going = false;

let serial;
let options = {
  baudRate: 9600
};
let portName = '/dev/cu.SLAB_USBtoUART';


function preload() {
  konkylie = loadSound('Konkylie.mp3');
}


function setup() {

  createCanvas(500,500);
  textSize(20);

  serial = new p5.SerialPort();
  serial.on('data', serialEvent);
  //serial.on('error', serialError);
  serial.open(portName, options);

  konkylie.setVolume(0.0);
  konkylie.playMode('sustain');

  console.log(startSum)
}

function draw() {
  background(0);

  if (!going){
    if ((leftVal + rightVal)> 920 && (leftVal + rightVal) < startSum){
      konkylie.play();
      going = true;
    }
  }

  //input management
  panning = map((leftVal-rightVal+5), -60, 60, -1, 1);
  if (panning == 0){
    volAdjustment = 0.001;
  } else if (panning < 0){
    volAdjustment = panning -(2*(panning));
  } else if (panning > 0){
    volAdjustment = panning
  }

  volume = map((leftVal + rightVal), startSum, 920, 0, 1) - (sq(volAdjustment)*0.4);

  push();
  fill(255);
  text("left input: " + leftVal, 20,20);
  text("right input: " + rightVal, 20, 50);
  text("pan input: " + (leftVal-rightVal+5), 20, 80);
  text("volume input: " + (leftVal+ rightVal), 20, 110);
  text("pan out: " + panning, 20, 140);
  text("volume out: " + volume, 20, 170);
  text(going, 20, 230);
  text(sq(volAdjustment)*0.4, 20, 200);
  pop();


  konkylie.pan(panning);
  konkylie.setVolume(volume);
}

function serialEvent() {
  var inString = serial.readStringUntil('\r\n');

  if (inString.length > 0) {
    var splitString = split(inString, "//");
    leftVal = Number(splitString[0]);
    rightVal = Number(splitString[1]);
  }
}
