
//movement variables for the lights
var or;
var og;
var ob;

//text box variables
var lineEnd = 0;
var boxRight;
var boxLeft;
var boxCount = 0;
var boxes = [];
var fadePos = 0;
//input text variabels
var textHeight = 18;
var textX;
var textInput = "";
var inputY;
var myFont;
var letterWidth;
//input / output variables
var humanTalk;
var computerTalk;

function preload(){
  myFont = loadFont ('assets/Courier New Bold.ttf')
}


function setup() {
  createCanvas(600, 500, WEBGL);
  frameRate(30);
  colorMode(HSB,360);

  inputY = 0+height/2-30;
  textX = 0-width/2+30;
  boxRight = width/2 - 50;
  boxLeft = 0-width/2 + 50;
}

function guide(){
  push();
  noStroke();
  ambientMaterial(360);
  translate(0 + map(or,-1,1,-200,150), 0 + map(or,-1,1,-200,20),10)
  sphere(20);
  pop();

  push();
  noStroke();
  specularMaterial(360);
  translate(0 + map(ob,-1,1,50,-100), 0 + map(ob,-1,1,-180,200), 10)
  sphere(20);
  ambientMaterial(360);
  pop();

  push();
  noStroke();
  translate(0 + map(og,-1,1,180,-230), 0 + map(og,-1,1,-230,200),10)
  sphere(20);
  pop();
}
function blurryBackground(){
  push();
  noStroke();
  ambientMaterial(360);
  plane(width,height,1);
  pop();

  //lights that change color
  push();
  or = cos(frameCount*0.01)
  og = cos(frameCount*0.014)
  ob = cos(frameCount*0.0179)
  ambientLight(0,0,240);
  //pink
  pointLight(340, 360, 360, 0 + map(or,-1,1,-200,150), 0 + map(or,-1,1,-200,20),200);
  //blue
  pointLight(230, 360, 360, 0 + map(ob,-1,1,50,-100), 0 + map(ob,-1,1,-180,200), 200);
  //green
  pointLight(90,360,360, 0 + map(og,-1,1,180,-230), 0 + map(og,-1,1,-230,300),100);
  ambientMaterial(360);
  push();
}
function mousePressed(){
  console.log(mouseX, mouseY)
  boxes[boxCount] = new computerTalkBox("A predefined message of some length", boxLeft, height/2-60);
  boxCount += 1;
  for(var i = 0; i < boxes.length; i++){
    boxes[i].update();
  }
}

//___________typing engine_____________
//_____________________________________
//_____________________________________
//_____________________________________
//_____________________________________
//_____________________________________
function keyPressed(){
  if(keyCode == BACKSPACE){
      textInput = textInput.substring(0, textInput.length - 1);
      if(lineEnd > 0){
        lineEnd -= letterWidth;
      }
  } else if (keyCode == ENTER){
      humanTalk = textInput;
      console.log("entered: "+humanTalk+": "+humanTalk.length);
      boxes[boxCount] = new talkBox(textInput, boxRight, height/2-60);
      boxCount += 1;
      for(var i = 0; i < boxes.length; i++){
        boxes[i].update();
      }
      textInput = "";
      textX = 0-width/2+30;
    }  //this is what happens when your press enter
}
function keyTyped (){
    textInput += key;
    if (keyCode != ENTER){
      if (lineEnd <= 259.2421875){
        lineEnd += letterWidth;
      } else {
        textX -= letterWidth * 1.3333;
      }
    }
  }

function draw() {
  background(60,360,360);
  blurryBackground();
  letterWidth = 10.8017578125;
  lineEnd = textWidth(textInput);


  textLeading(20);
  textFont(myFont);
  textSize(textHeight);
  //textStyle(BOLD);

  push();
  textAlign(TOP, LEFT)
  fill(0,0,100);
  text(textInput, textX, inputY);
  pop();

  //blinker
  push();
  var blinkerAlpha;
  if (frameCount%20<10){
    blinkerAlpha = 0;
  } else {
    blinkerAlpha = 360;
  }
  var blinkX = 0-width/2 + 30 + lineEnd;
  var blinkY = height/2 - 30 - textHeight;

  noStroke();
  fill(0,0,100,blinkerAlpha);
  rect(blinkX, blinkY, 11, textHeight);
  pop();

  //controls box behavior
  if (boxCount > 0){
    for(var i = 0; i < boxes.length; i++){
      boxes[i].create();
      boxes[i].dissolve();
    }
  }
  console.log("boxCount: "+boxCount,)
}

//________text field objects___________
//_____________________________________
//_____________________________________
//_____________________________________
//_____________________________________
//_____________________________________
class talkBox {
  constructor(_inputString, _x, _y){
    this.inputString = _inputString;
    this.x = _x;
    this.y = _y;
    this.letterX;
    this.letterY;
    this.boxID = boxCount;
    this.alpha = 360;
    this.dissolving = false;
  }
  create(){
    push();
    textAlign(RIGHT, TOP);
    fill(0,0,100,this.alpha);
    if (this.inputString.length > 30){
      push();
      textAlign(RIGHT, TOP);
      if (this.dissolving){
      }else{
        text(this.inputString, this.x-360, this.y, 350);
      }
      pop();
    } else{
      if (this.dissolving){
      }else{
        text(this.inputString, this.x, this.y);
      }
    }
    pop();
  }
  update(){
    if (this.dissolving){
    }else {
      this.y = this.y - (boxCount - this.boxID)*10-20;
    }
  }
  dissolve(){
    if(this.y < fadePos){
      this.dissolving = true;
      this.alpha = this.alpha * 0.96;
      this.y = this.y*1.01;

      var letters = Array.from(this.inputString);
      for(var i = 0; i<=this.inputString.length; i++){
        this.letterX = this.x - this.inputString.length * letterWidth + i * letterWidth;

        text(letters[i], this.letterX, this.y);
      }
    }
  }
}

class computerTalkBox {
  constructor(_inputString, _x, _y){
    this.inputString = _inputString;
    this.x = _x;
    this.y = _y;
    this.boxID = boxCount;
    this.alpha = 360;
    this.dissolving = false;
  }
  create(){
    push();
    textAlign(LEFT, TOP);
    fill(0,0,100,this.alpha);
    text(this.inputString, this.x, this.y, 300)
    pop();
  }
  update(){
    if (this.dissolving){
    }else {
      this.y = this.y - (boxCount - this.boxID)*10-20;
    }
  }
  dissolve(){
    if(this.y < fadePos){
      this.dissolving = true;
      this.alpha = this.alpha * 0.96;
      this.y = this.y*1.01;

      var letters = Array.from(this.inputString);
      for(var i = 0; i<=this.inputString.length; i++){
        this.letterX = this.x + i * letterWidth;

        text(letters[i], this.letterX, this.y, 300);
      }
    }
  }
}
