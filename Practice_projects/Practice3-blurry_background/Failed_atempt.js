let dim;

function setup() {
  createCanvas(600, 400);
  colorMode(HSB, 360, 100, 100, 100);
  dim = width / 2;
  noStroke();
  ellipseMode(RADIUS);
  frameRate(1);
}

function draw() {
  background(0,0,80);
    drawGradient(mouseX, height / 2,90);

}

function drawGradient(x, y, h) {
  let radius = 400;
  let o = 0;
  for (let r = radius; r > 0; --r) {
    fill(h, 100, 100, o);
    ellipse(x, y, r, r);
    h += 0.02;
    o += 0.0025;
  }
}
