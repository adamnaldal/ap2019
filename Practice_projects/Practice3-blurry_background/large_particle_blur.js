var blurParticles = [];
var bgHueBase;
var bgHue;
var input;

function setup() {
  createCanvas(600, 400);
  colorMode(HSB, 360, 100, 100,100);
  frameRate(30);

  for (let i = 0; i<600; i++){
    var x = random(width);
    var y = random(height);
    var r = random(20,35);
    var hue = random(-30,30);
    blurParticles[i] = new blurParticle(x, y, r, hue)
  }
  input = createInput();
  input.changed(newText);
}

function newText(){
  createP(input.value());
}

function draw() {
  var bgHueBase = cos(frameCount*0.01);
  var bgHue = map(bgHueBase,-1,1,190,335);
  background(0,0,100,80);

  for (let i=0; i<blurParticles.length; i++){
    blurParticles[i].show(bgHue);
    blurParticles[i].move();
  }

}

class blurParticle {
  constructor(x,y,r,hue,change){
    this.x = x;
    this.y = y;
    this.r = r;
    this.hue = hue;
  }
  show(input){
    noStroke();
    fill(input+this.hue,50,80,2);
    ellipse(this.x, this.y, this.r*2);
  }
  move(){
    this.x = this.x + random(-0.5,0.5)
    this.y = this.y + random(-0.5,0.5)
  }
}
