//background color variables
var inc = 0.008
var start = 0;
var zoff = 0;
var coff = 0;
var h;
//text box variables
var lineEnd = 0;
var boxRight;
var boxLeft = 50;
var boxCount = 0;
var boxes = [];
var fadePos = 200;
//input text variabels
var textHeight = 16;
var textX = 20;
var textInput = "";
var inputY;
//input / output variables
var humanTalk;
var computerTalk;

function setup() {
  createCanvas(400, 450);
  pixelDensity(1);
  colorMode(HSB,360,100,100,100);
  noiseDetail(4,0.25);
  frameRate(30);

  inputY = height - 20;
  boxRight = width - 50;
}

function blurryBackgroundOverlay(){
  h = map(noise(coff),0,1,160,320);
  background(h,70,80,100);

  loadPixels();
  var yoff = 0;
  for (var y = 0; y < height; y ++){
    var xoff = 0;
    for (var x = 0; x < width; x ++){
      var index = (x + y * width) * 4;
      var r = map(noise(xoff, yoff, zoff),0,1,5,80);
      pixels[index+3] = r;
      xoff += inc;
    }
    yoff += inc;
  }
  zoff += inc*2;
  coff += inc*6;
  updatePixels();
}


function draw() {

  if (frameCount % 2 == 0){
    blurryBackgroundOverlay();
  } // Draws the blurry background

  textLeading(20);
  textFont('Courier')
  textSize(textHeight);


  push();
  textAlign(BOTTOM, LEFT)
  fill(0,0,30,100);
  noStroke();
  text(textInput, textX, inputY);
  pop();

  //blinker
  push();
  var blinkerAlpha;
  if (frameCount%20<10){
    blinkerAlpha = 0;
  } else {
    blinkerAlpha = 100;
  }
  var blinkX = 20 + lineEnd * 1.3333;
  var blinkY = height - 20 - textHeight;

  noStroke();
  fill(0,0,30,blinkerAlpha);
  rect(blinkX, blinkY, 12, 18);
  pop();


  if (boxCount > 0){
    for(var i = 0; i < boxes.length; i++){
      boxes[i].create();
      boxes[i].dissolve();
    }
  }
    console.log(lineEnd, textInput, textX);
}


//___________typing engine_____________
//_____________________________________
//_____________________________________
//_____________________________________
//_____________________________________
//_____________________________________

function keyPressed(){
  if(keyCode == BACKSPACE){
      textInput = textInput.substring(0, textInput.length - 1);
      lineEnd -= 7.201171875;
  } else if (keyCode == ENTER){
      humanTalk = textInput;
      console.log("entered: "+humanTalk+humanTalk.length);
      boxes[boxCount] = new talkBox(textInput, boxRight, height-60);
      boxCount += 1;
      for(var i = 0; i < boxes.length; i++){
        boxes[i].update();
      }
      textInput = "";
      lineEnd = 0;
      textX = 10;
    }  //this is what happens when your press enter
}

function keyTyped (){
    textInput += key;
    if (keyCode != ENTER){
      if (lineEnd <= 259.2421875){
        lineEnd += 7.201171875;
      } else {
        textX -= 7.201171875 * 1.3333;
      }
    }
  }

function mousePressed(){
  console.log(mouseX, mouseY)
  boxes[boxCount] = new computerTalkBox("A predefined message of some length", boxLeft, height-60);
  boxCount += 1;
  for(var i = 0; i < boxes.length; i++){
    boxes[i].update();
  }
}

//________text field objects___________
//_____________________________________
//_____________________________________
//_____________________________________
//_____________________________________
//_____________________________________
class talkBox {
  constructor(_inputString, _x, _y){
    this.inputString = _inputString;
    this.x = _x;
    this.y = _y;
    this.boxID = boxCount;
    this.alpha = 100;
    this.dissolving = false;
  }
  create(){
    push();
    textAlign(RIGHT, BOTTOM);
    fill(0,0,30,this.alpha);
    if (this.inputString.length > 30){
      push();
      textAlign(RIGHT);
      text(this.inputString, this.x-360, this.y, 350);
      pop();
    } else{
      text(this.inputString, this.x, this.y);
    }
    pop();
  }
  update(){
    if (this.dissolving){
    }else {
      this.y = this.y - (boxCount - this.boxID)*10-20;
    }
  }
  dissolve(){
    if(this.y < fadePos){
      this.dissolving = true;
      this.alpha = this.alpha * 0.98;
      this.y -= this.y/300;

      // var letters = [];
      // for(var i = 0; i<=this.inputString.length; i++){
      //   letters[i] = this.inputString.substring(i,i)
      //   text(letters[i], this.x - i * 4, this.y - i * 3);
      // }
    }
  }
}

class computerTalkBox {
  constructor(_inputString, _x, _y){
    this.inputString = _inputString;
    this.x = _x;
    this.y = _y;
    this.boxID = boxCount;
    this.alpha = 100;
    this.dissolving = false;
  }
  create(){
    push();
    textAlign(LEFT, BOTTOM);
    fill(0,0,30,this.alpha);
    text(this.inputString, this.x, this.y, 300)
    pop();
  }
  update(){
    if (this.dissolving){
    }else {
      this.y = this.y - (boxCount - this.boxID)*10-20;
    }
  }
  dissolve(){
    if(this.y < fadePos){
      this.dissolving = true;
      this.alpha = this.alpha * 0.98;
      this.y -= this.y/300;
    }
  }
}
