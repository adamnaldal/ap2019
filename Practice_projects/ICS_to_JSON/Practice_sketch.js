let filePicker;
let output = [];
let sortedOutput;
let fileTitle = "4_semester_foreløbigt"
let semester = 4;
let data;
let bearButton;

Array.prototype.sortBy = function(p) {
  return this.slice(0).sort(function(a, b) {
    return (a[p] > b[p]) ? 1 : (a[p] < b[p]) ? -1 : 0;
  });
}

function setup() {
  filePicker = createFileInput(jsonifyEvents);
  bearButton = createButton('Create bear notes');
  bearButton.mouseClicked(createNotes)
  //window.open("bear://x-callback-url/create?title=My%20Note%20Title&text=First%20line&tags=home,home%2Fgroceries")
}

function jsonifyEvents(singleString) {
  let data = str(split(singleString.data, "\n"))

  //global for loop
  for (let i = 0; i < data.length; i++) {

    //single event creator
    if (data[i].includes("BEGIN:VEVENT")) {
      let newEvent = {};

      //finds and adds subject
      let lookingForSubject = true;
      for (let j = i; lookingForSubject; j++) {
        let foundSubject = match(data[j], "SUMMARY:.*");
        let endedSubject = match(data[j], "END:VEVENT");
        if (foundSubject) {
          newEvent.subject = split(foundSubject[0], ':')[1]
          lookingForSubject = false;
        } else if (endedSubject) {
          lookingForSubject = false;
        }
      } //subject finder end

      //finds and adds date
      let lookingForDate = true;
      for (let j = i; lookingForDate; j++) {
        let foundDate = match(data[j], "DTSTART:.*");
        let endedDate = match(data[j], "END:VEVENT");
        if (foundDate) {
          let rawDate = split(foundDate[0], ':')[1]
          let monthNr = rawDate.substring(4, 6)
          let dayNr = rawDate.substring(6, 8)
          let month;
          let alfabetize = function() {
            if (monthNr == '01') {
              month = "Jan"
            } else if (monthNr == '02') {
              month = "Feb"
            } else if (monthNr == '03') {
              month = "Mar"
            } else if (monthNr == '04') {
              month = "Apr"
            } else if (monthNr == '05') {
              month = "Maj"
            } else if (monthNr == '06') {
              month = "Jun"
            } else if (monthNr == '07') {
              month = "Jul"
            } else if (monthNr == '08') {
              month = "Aug"
            } else if (monthNr == '09') {
              month = "Sep"
            } else if (monthNr == '10') {
              month = "Okt"
            } else if (monthNr == '11') {
              month = "Nov"
            } else if (monthNr == '12') {
              month = "Dec"
            } else {
              month = monthNr
            }
          }
          alfabetize();

          newEvent.date = dayNr + " " + month
          newEvent.rawDate = rawDate.substring(0, 8)
          lookingForDate = false;
        } else if (endedDate) {
          lookingForDate = false;
        }
      } //date finder end

      //finds and adds texts from description seperatde by "§"
      let lookingForTexts = true;
      for (let j = i; lookingForTexts; j++) {
        let foundTexts;
        let textToMatch;

        if (match(data[j], "DESCRIPTION:.*") != null) {
          textToMatch = data[j] + data[j + 1] + data[j + 2] + data[j + 3] + data[j + 4] + data[j + 5]
          foundTexts = match(textToMatch, "DESCRIPTION:((?!END|STATUS|URL)[\\s\\S])*");
        }

        let endedTexts = match(data[j], "END:VEVENT");
        if (foundTexts) {
          let textString = split(foundTexts[0], ':')[1]
          let cleanTextString = textString.replace(/\\n|\\/g, "");
          //console.log(cleanTextString)
          let textArray = split(cleanTextString, "§ ");

          if (textArray[0] != "Type" && textArray[0] != "\r")
            newEvent.texts = textArray
          lookingForTexts = false;
        } else if (endedTexts) {
          lookingForTexts = false;
        }
      } //Text finder end

      output.push(newEvent)
    } //single event creator end
  } //global for loop end

  //sorts and outputs
  sortedOutput = output.sortBy('rawDate');
  if (output.length > 0) {
    saveJSON(sortedOutput, fileTitle + ".json");
  } else {
    console.error("no events found");
  }

} //jsonifyEvent

//creates bear notes with index for each object in sorted Output
function createNotes() {
  for (let i = 0; i < sortedOutput.length; i++) {

    //–—–––––––––Title (mostly)–––––––––––––

    let headerElements = []; // an array of short versions of the texts for the title, url encoded
    let readingElements = []; //an array of the texts, url encoded

    //populates headerElements[] and readingElements[]
    if (sortedOutput[i].texts) {
      for (let each of sortedOutput[i].texts) {
        var headElement = encodeURIComponent(each).replace(/%0D%20/g, "");

        readingElements.push(headElement.replace(/%0D/g, ""));

        if (headElement.includes(")")) {
          headElement = headElement.match(/.+?\)/);
          headerElements.push(headElement[0]);
        } else {
          headerElements.push(headElement)
        }
      }
    } else {
      headerElements.push("No Texts");
    }

    console.log(readingElements, headerElements)
    //collects the header elements into one string and puts together the full title with the date and texts
    let header = headerElements.join(encodeURIComponent(" & "));
    let title = encodeURIComponent(sortedOutput[i].date + " – ") + header;

    //–––––––––––––Index and Body––––––––––––––

    let text;

    let indexElements = []
    let body = "";
    if (sortedOutput[i].texts) {
      for (let j = 0; j < sortedOutput[i].texts.length; j++) {

        let indexElement = (j + 1) + ". [Tekstnoter – " + decodeURIComponent(readingElements[j]) + "](bear://x-callback-url/open-note?title=" + title.replace(/\)/g, "%29").replace(/\(/g, "%28") + "&header=Tekstnoter%20%E2%80%93%20" + readingElements[j].replace(/\)/g, "%29").replace(/\(/g, "%28") + ")";

        let pdfHeader = "\t* [Tekst " + (j + 1) + "](bear://x-callback-url/open-note?title=" + title.replace(/\)/g, "%29").replace(/\(/g, "%28") + "&header=Tekst%20" + (j + 1) + ")"

        body = body + "## Tekstnoter – " + decodeURIComponent(readingElements[j]) + "\n* \n\n" + "### Tekst " + (j + 1) + "\n\n__\n"

        indexElements.push(indexElement);
        indexElements.push(pdfHeader)
        console.log(indexElement)
      }
    } else {
      indexElements.push("No Texts");
    }

    text = encodeURIComponent(indexElements.join("\n") + "\n\n" + body)
    console.log(text)


    //–––––––––––––Tags–––––––––––––––

    let tags = encodeURIComponent("uni/" + semester + "/" + sortedOutput[i].subject);

    console.log("bear://x-callback-url/create?&show_window=no&title=" + title + "&text=" + text + "&tags=" + tags)

    window.open("bear://x-callback-url/create?&show_window=no&title=" + title + "&text=" + text + "&tags=" + tags)
  }
}
