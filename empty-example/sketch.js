function setup() {
  createCanvas(600,500);
  frameRate();
}

function draw() {
  background(mouseY,100,mouseX);
  circle(mouseX,mouseY,60);
  fill(mouseX,mouseX,mouseY);
  circle(pmouseX,pmouseY,30);
  fill(mouseY,mouseX,mouseX);
  square(100,10,120);
  fill(360,50,0);
}

function mouseMoved() {
  ellipse(mouseX, mouseY, 20, 20);
  return false;
}
