## Mini exercise 8
![A murder vicitm](Screenshot.png)

[link](https://glcdn.githack.com/adamnaldal/ap2019/raw/master/Mini_Exercises/mini_ex8/index.html)

### Description
For this weeks mini exercise i worked with Thomas.
Our program takes one of a number of fifty-word horror stories and replaces the nouns in them with random nouns.
The idea is to take what is a very suspenseful story and deflate the dramatic tension by changing certain words without any regard to the story as a whole (because the computer doesn't know about things like suspense and horror).

When the program launches the user is asked to turn on the sound. This is because we use both background sounds as well as sound effects and voice synthesis.
On a technical level we have a json file with horror stories that we found on the internet. It is formatted in a strange way because we had to separate the punctuation marks into their own index for the mapping algorithm we use to get the x and y values of each word later on.
Whenever the user presses the "execute" button we replace the nouns with random nouns and overlays them in a red gothic font to emphasise the "horror" of ruining a suspenseful story in this way. We then use the p5.speech library to narrate the new story with speech synthesis further emphasising the lack of awareness of things like pace and suspense on the part of the computer.
When the "new victim" button is pressed the previous red words and their related data(x and y positions as well as the width of the word) are cleared and a random story is pulled from the json file, making the program ready to go again with a new story.

The collaborative was pretty straight forward and we quickly started working on different aspects of the program simultaneously, which made it possible for us to work faster and get a more complicated program done on time. It was also nice to have someone else to spar with about different details of the program as well as the conceptual elements of the program.

### Putting it into perspective
The program plays with the textual conventions of the horror genre by taking a perfectly good, well structured horror story and getting the computer to change it in a way that really destroys the suspense and meaning of the story. One of the ideas we wanted to explore was how humans make sense of stories and how making sense of language is something that is famously hard for computers. So we thought one of the best ways of doing it was presenting a story and the presenting a slightly altered version of it that doesn't make any sense thus pointing out the fact that making sense of a sentence is about more than grammar and word definitions.

More than just playing with the textual tradition of horror stories we wanted to also play with the oral tradition of telling stories around a campfire and so we added an eerie background noise that should evoke a campfire in a dark stormy night and made the program read the changed (and often nonsensical) story aloud. This of course is also something that the computer does quite well at in the sense that most of the words are intelligible and it even applies proper intonation before question-marks. But at the same time it doesn't "orate" in the same way a human could because it has no concept of building suspense and changing volumes for dramatic effect and so on.

In the code we wanted to try our hands on codework and so we decided to make a double play on the "murder" or "slaughter" of a "victim"(the story). Therefore our variables are named things like "creatures" (for the json with stories), "bodyParts" (for the array of individual words in the current story) and our functions are called things like "dissect", "mutateCorps" and "showMutilatedBody". Thus the code tells it's own meta morror story about a story that is "executed" (as in murdered) and "frankensteined" back together as a horrible shadow of its former self.
