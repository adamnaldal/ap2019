//variables
let creatures;
let victim;
let bodyParts = [];
let mutantParts = [];
let imagesOfTheLivingDead = {};
let livingImage;
let cage;
let execute;
let newVictim;
let newLook;
let anatomicalMap = [];
let narrator;
let backgroundNoise;
let lightningStrike;
let screamVictim;


function preload() {
  creatures = loadJSON('creatures.json');
  newLook = loadFont("PERRYGOT.TTF")

  backgroundNoise = loadSound('sounds/backgroundNoise.mp3');
  lightningStrike = loadSound('sounds/lightningStrike.wav');
  screamVictim = loadSound('sounds/screamVictim.mp3')
}

function setup() {
  backgroundNoise.setVolume(0.9)
  backgroundNoise.loop();
  createCanvas(window.innerWidth, window.innerHeight);
  cage = width / 4;
  //creates button for executing function mutateCorps. The function can be called multiple times on the same victim.
  execute = createButton('Execute')
  execute.position(cage, height / 3 + 250)
  //Class styled in style.css
  execute.addClass('FrankStone')
  execute.mousePressed(mutateCorps)
  //creates button for loading a new victim, will choose from the "creatures" array at random.
  newVictim = createButton('New Victim')
  newVictim.position(cage + 200, height / 3 + 250)
  newVictim.class('FrankStone')
  newVictim.mousePressed(summonVictim);

  //this creates the narrator voice
  narrator = new p5.Speech();
  narrator.setPitch(0.65);
  narrator.setRate(0.73);

  //this chooses a new horror story from our json file
  summonVictim();

  textSize(14);
  textAlign(LEFT, TOP)
}

function draw() {
  background(10);

  //this creates the text that tells the user to turn on the sound so they can hear the narration.
  push()
  textAlign(CENTER);
  textSize(32);
  fill(255, 255 - frameCount);
  textFont(newLook);
  text("Please turn on sound.", width / 2, 80);
  pop()

  //this displays the horror story
  fill(255)
  text(livingImage, cage, height / 3, width - cage - cage, 300);

  //this overlays the new words
  showMutilatedBody();
}

//this clears the current red words and their related data and picks a new story to display
function summonVictim() {
  imagesOfTheLivingDead = {};
  bodyParts = [];
  anatomicalMap = [];
  narrator.cancel();

  victim = creatures.souls[floor(random(creatures.souls.length))]

  disect(victim);
  livingImage = join(victim, "")

  if (frameCount > 1) {
    screamVictim.setVolume(0.05);
    screamVictim.play();
    console.log("scream")
  }
}

//splits the chosen horror stories into discret words and puts them in the two dimenional array called bodyParts
function disect(subject) {
  for (let chunk of subject) {
    bodyParts.push(split(chunk, " "));
  }
}

//this is called when you push the execute button. It finds all of the nouns (singular og plural) (but avoids nouns ending in -"ing" because it confuses a lot of verbs that way) and replaces them with random nouns.
function mutateCorps() {
  lightningStrike.play(0, 1, 0.25, 4.5, 2.5);
  //this clears any words that were generated last time mutateCorps was called.
  mutantParts = [];
  narrator.cancel();

  //these loops loop through each word in bodyParts
  for (let organs = 0; organs < bodyParts.length; organs++) {
    //this is the line level (organ means line)

    let organ = bodyParts[organs];
    for (let pieces = 0; pieces < organ.length; pieces++) {
      //this is the word level (organ[pieces] means words)

      //here we detect nouns using the RiTa library
      if ((RiTa.getPosTags(organ[pieces]) == "nn" || RiTa.getPosTags(organ[pieces]) == "nns") && organ[pieces].endsWith("ing") == false) {

        //here we replace nouns with new, random ones
        organ[pieces] = RiTa.randomWord(RiTa.getPosTags(organ[pieces])[0]);

        //here we make an array that contains only the randomly generated nouns as empty objects (im pretty sure this is not the best way to do it but i works).
        imagesOfTheLivingDead[organ[pieces]] = {};

        //here we replace the old nouns with the new ones in the bodyParts array so that the text will move according to the width of the new nouns
        bodyParts[organs][pieces] = organ[pieces];
      }
    }

    //this makes an array of the story that is equivalent to the original json in format but with the new words
    mutantParts.push(join(bodyParts[organs], " "));

  }
  //this changes the source of the white text on the screen to the new text with the new words so that when we overlay the new words in imagesOfTheLivingDead the text wil move acordingly.
  livingImage = join(mutantParts, "");
  //this stores the x and y values of the starting points of each word in bodyparts.
  mapAnatomy()

  //this makes the narrator speak the new story with a slight delay
  setTimeout(tellStory, 2200);
}

//this function stores the position of each word in bodyParts and their width in pixels. This is used later to block out each noun with a black rectangle and place the randomly selected word on top in the correct position.
function mapAnatomy() {
  let xs = cage;
  let ys = height / 3;
  let ybonus = 0;
  let xbonus = 0;
  anatomicalMap = [];

  for (let lines of bodyParts) {
    //this variable will store the x,y,and width of the word until it is pushed to the array
    let region = [];
    for (let word of lines) {

      //this feeds the x and y position the values for the current word
      let x = xs + xbonus;
      let y = ys + ybonus;

      //move down the y value one line and reset the x position to zero if the line changes
      if (x + textWidth(word) > width - cage || word == "\n") {
        ybonus += textLeading();
        xbonus = 0;
      }

      //store the x,y and width temporarily
      region.push([x, y, textWidth(word)]);

      //move right the width of the word.
      if (word != "" && word != "\n") {
        if (RiTa.isPunctuation(word)) {
          xbonus += textWidth(word);
        } else {
          xbonus += textWidth(word + " ");
        }
      }
    }

    //add the current line of values
    anatomicalMap.push(region);
  }
}

//this diplays the red words
function showMutilatedBody() {
  //loops through each word in bodyParts
  if (imagesOfTheLivingDead) {
    for (let lines of bodyParts) {
      for (let words of lines) {

        //if the current word matches one of the changed words create black rectangle to block out the white text and write over it with red text in a different font.
        if (imagesOfTheLivingDead[words]) {
          let indexA = bodyParts.indexOf(lines)
          let indexB = lines.indexOf(words)

          //draws rectangle
          push();
          noStroke();
          fill(10);
          rect(anatomicalMap[indexA][indexB][0], anatomicalMap[indexA][indexB][1], anatomicalMap[indexA][indexB][2], textSize() + textDescent())
          pop();

          //draws text
          push();
          textFont(newLook);
          textSize(10);
          fill(240, 40, 0)
          text(words, anatomicalMap[indexA][indexB][0], anatomicalMap[indexA][indexB][1] + textSize() / 2);
          pop();
        }
      }
    }
  }
}

//this makes the narrator say the story
function tellStory() {
  narrator.speak(livingImage)
}