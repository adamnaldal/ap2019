//thanks to Joseph Huckaby for the use of the webcamjs library

let w = 0;
let h = 0;

//variables related to positioning and moving the pupil
let pupilX;
let pupilY;
let easing = 0.2;
let v1;
let pupilHeading;
let diameterx;
let diametery;
let xMax;
let yMax;
let targetX;
let targetY;

//variables related to blinking
let blinking = false;
let iy = 40;
let i = 0;

//variables related to capturing and positioning videos on the screen
let camera;
let inums = 0;
let imgs = [];
let screenSub = 5;
let xPic;
let yPic;

function setup() {
  w = windowWidth;
  h = windowHeight;
  pupilX = w / 2;
  pupilY = h / 2;
  createCanvas(w, h);
  frameRate(30);
  angleMode(DEGREES);
  noStroke();

  //this uses the webcamjs library to create a video feed that the vido frames will be created from when the ey blinks. and hides the original
  camera = createDiv().size(width / screenSub, height / screenSub);
  camera.id('my_camera')
  Webcam.attach('#my_camera');
  camera.hide();

  //this determines how often the eye blinks (every 6000 milliseconds or every 6 seconds)
  setInterval(() => {
    blinking = true;
  }, 6000);
}

function draw() {
  background(10);

  // figures out the angle form the center of the eye to the mouse position
  v1 = createVector(mouseX - width / 2, mouseY - height / 2)
  pupilHeading = v1.heading()
  //defines the area that the pupil can move within as an ellipse using some trigonometry
  diameterx = 40;
  diametery = 24;
  xMax = diameterx * cos(pupilHeading);
  yMax = diametery * sin(pupilHeading);

  //Constrains pupil to the ellipse defined above if the mouse is outside said ellipse and makes it move to the mouse if the mouse is inside said ellipse.
  if (dist(width / 2, height / 2, mouseX, mouseY) >= dist(width / 2, height / 2, width / 2 + xMax, height / 2 + yMax)) {
    targetX = width / 2 + xMax;
    targetY = height / 2 + yMax;
  } else {
    targetX = mouseX;
    targetY = mouseY;
  }
  //this makes the eye move more slowly towards the mouse the closer it is to the mouse/edge of the eye
  let dx = targetX - pupilX;
  pupilX += dx * easing;
  let dy = targetY - pupilY;
  pupilY += dy * easing;

  //draws the white of the eye
  push();
  fill(255);
  ellipse(width / 2, height / 2, 100);
  pop();

  //Draws pupil
  c = color(10);
  fill(c);
  ellipse(pupilX, pupilY, 50, 50);

  //calls the blink function
  blink();

  //this draws the eyelids
  push();
  beginShape();
  translate(width / 2, height / 2);
  fill(10);
  beginShape();
  for (let i = 0; i < 360; i++) { //this just draws a black outer circle around the eye
    let x = 60 * cos(i);
    let y = 55 * sin(i);
    vertex(x, y);
  }
  beginContour(); //this draws the inner eyelids
  vertex(+45, 0);
  bezierVertex(+27, 0 - iy, -27, 0 - iy, -45, 0);
  bezierVertex(-27, 0 + iy, +27, 0 + iy, +45, 0);
  endContour();
  endShape();
  pop();
}

//blinks when mouse is pressed within the same ellipse as before that roughly corresponds to the eyeball
function mousePressed() {
  if (dist(width / 2, height / 2, mouseX, mouseY) <= dist(width / 2, height / 2, width / 2 + xMax, height / 2 + yMax)) {
    blinking = true;
  }
}

//makes the eye blink if blinking is set to true, either by mousePressed or setInterval. The eyelids will close, the pupil will reset to the center of the eye and the eyelids will open again.
function blink() {
  if (blinking) {
    //this controls the iy value which describes the distance of the top and bottom most part of the eyelids to the center of the eyes
    const speed = 10;
    const iterate = 40 * 2 / speed;
    if (i < iterate / 2) { //the eye closes
      iy = iy - speed - speed;
      i++;
    } else if (i < iterate) { //the eye opens again
      iy = iy + speed + speed;
      i++;
    } else { // and blinking is set to false again
      blinking = false;
      i = 0;
      iy = 40;
      newPic();
    }
    if (i == iterate / 2) { // this resets the pupil to the center of the eye
      pupilX = w / 2;
      pupilY = h / 2;
    }
  }
}

// this function creates a new video "picture"
function newPic() {
  imgs.push(createDiv().size(width / screenSub, height / screenSub));
  imgs[inums].id('my_camera' + inums);
  Webcam.attach('#my_camera' + inums);

  // this makes sure the images don't overlap with the eye
  let q = ceil(random(4));

  if (q == 1) {
    xPic = random(width / 2);
    yPic = random(height / 2 - (diametery + 10) - height / screenSub);
  } else if (q == 2) {
    xPic = random(width / 2 + (diameterx + 10), width - width / screenSub + 1);
    yPic = random(height / 2);
  } else if (q == 3) {
    xPic = random(width / 2 - (diameterx + 10) - width / screenSub);
    yPic = random(height / 2 - height / screenSub, height - height / screenSub + 1);
  } else {
    xPic = random(width / 2 - width / screenSub, width - width / screenSub + 1);
    yPic = random(height / 2 + (diametery + 10), height - height / screenSub + 1);
  }

  //this positions the picture and increments the variable that decides which picture is being modified
  imgs[inums].position(xPic, yPic);
  inums++;
}