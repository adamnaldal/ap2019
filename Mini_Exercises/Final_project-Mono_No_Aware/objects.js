class branch {
  constructor(begin, end) {
    this.begin = begin; //starting point of the branch, stored as vector
    this.end = end; //end point of the branch stored as vector
    this.middlePoints = []; //an array containing the middle points of the branch
    this.length = dist(this.begin.x, this.begin.y, this.end.x, this.end.y) // the length of the branch
    this.sections = 5; //number of lines in the branch
    this.variation = this.length / 8; //the amount that the points in the middle can deviate from the straight line between this.begin and this.end as a ratio of the lenght of the branch.
    this.thickness = this.length / 6; //the weight of the line, which depends on the length of the branch.
    this.direction = p5.Vector.sub(this.end, this.begin); //the direction of the branch on the x and y axis.
    this.i = 1; //the iterator that increases each time the branch gets a new middlepoint
    this.hasSplit = false; //the variable that determines if a branch is still growing or has split into two smaller branches.

    //creates the first branch
    while (this.i < 2) {
      this.extendBranch();
    }
  }

  //draws the branch on the screen
  show() {
    push();
    noFill();
    strokeWeight(this.thickness);
    stroke(40, 30, 20);

    beginShape()
    //displays the beginning of the branch
    vertex(this.begin.x + xOffset, this.begin.y + yOffset)

    //displays the midpoints along the branch
    for (let each of this.middlePoints) {
      vertex(each.x + xOffset, each.y + yOffset);
    }

    //displays the end if the branch is fully grown
    if (this.i == this.sections) {
      vertex(this.end.x + xOffset, this.end.y + yOffset)
    }
    endShape()
    pop();
  }

  extendBranch() {
    //adds a new middlepoint to the branch if it has fewer middle points than sections
    if (this.i < this.sections) {
      //the "seed for the noise". this is detirined independantely for the x and y value of each point
      let noiseStartX = random(20);
      let noiseStartY = random(20);

      //generates the x coordinate for the new point which lies in the direction of the end point but with some noise added
      let x = this.begin.x + this.i * (this.direction.x / this.sections) + map(noise(noiseStartX + this.i), 0, 1, -this.variation, this.variation);

      ////generates the y coordinate for the new point which lies in the direction of the end point but with some noise added
      let y = this.begin.y + this.i * (this.direction.y / this.sections) + map(noise(noiseStartY + this.i), 0, 1, -this.variation, this.variation);

      //this takes the x and y coordinates and puts them into a vector which is added to the array middlePoints
      let point = createVector(x, y);
      this.middlePoints.push(point);

      //creates flowers if the tree has more than 6 branches
      if (tree.length >= 7) {
        //bloom represents the chance of flowers forming. this chance decreases exponentially the more branches there are on the tree so as not to overcrowd the screen with to many flowers
        let bloom = random(map(sqrt(tree.length), 0, 120, 0, 5));

        //makes between one and three flowers and places them randomly within an 8 by 8 pixel cube of the fursthes visible point on the branch
        if (!this.hasSplit && bloom < 0.1) {
          let numberOfFlowers = floor(random(1, 4))
          for (let i = 0; i < numberOfFlowers; i++) {
            flowers.push(new flower(this.middlePoints[this.i - 1].x + random(-8, 8), this.middlePoints[this.i - 1].y + random(-8, 8)))
          }
        }
      }

      this.i++
    } else {
      //if the branch has enough midpoints but hasn't split yet it will split otherwise nothing happens.
      if (!this.hasSplit) {
        let newBranch = this.split()
        tree.push(newBranch[0], newBranch[1])
        this.hasSplit = true;
      }
    }
  }

  //crates two new branches with endpints that fan out randomly form the end of the current branch.
  split() {

    //creates the end point in in a random direction between -20 and 60 degrees (from the angle of the current branch) for the first branch. The new branch extends 75% of the length of the current branch from the end of the current branch.
    let dir1 = p5.Vector.sub(this.end, this.begin);
    dir1.rotate(random(-20, 60));
    dir1.mult(0.75)
    let newEnd1 = p5.Vector.add(this.end, dir1);

    //creates the end point in in a random direction between -60 and 20 degrees (from the angle of the current branch) for the first branch. The new branch extends 75% of the length of the current branch from the end of the current branch.
    let dir2 = p5.Vector.sub(this.end, this.begin);
    dir2.rotate(random(-60, 20));
    dir2.mult(0.75)
    let newEnd2 = p5.Vector.add(this.end, dir2);

    //creates and returns the two new branches so they can be passed to the tree array.
    let sprouts = []
    sprouts.push(new branch(this.end, newEnd1));
    sprouts.push(new branch(this.end, newEnd2));
    return sprouts
  }
}

class flower {
  constructor(x, y) {
    this.y = y //the center of the flower
    this.x = x //the center of the flower
    this.d = 5; //the width the central circle of the flower
    this.r = random(360); //orients the at a random rotation to make them look unique
    this.falling = true; //used to determine if the flower is still on the tree.
    this.petals = []; // the array that stores the 5 pettals around the central circle.
    //pushes 5 petal objects to the petals array.
    for (let i = 0; i < 5; i++) {
      this.petals.push(new petal(5.8 * cos(i * (360 / 5)), 5.8 * sin(i * (360 / 5)), i * (360 / 5)));
    }
  }

  show() {
    push();
    translate(this.x + xOffset, this.y + yOffset)
    rotate(this.r)

    //draws the petals
    noStroke();
    for (let each of this.petals) {
      each.show();
    }
    //draws the center of the flower
    fill(flowerBudColor);
    ellipse(0, 0, this.d);
    pop();
  }

  //makes the flower fall downwards (if it hasn't reached the table) and to the right using sin() to simulate wind.
  fall() {
    if (this.falling) {
      if (this.y + yOffset < random(height - 90, height - 10)) {
        this.y += (60 / frameRate());
        this.x += sin((frameCount + this.r) * 2) / 2 + (60 / frameRate() * 0.5)
      } else {
        this.falling = false;
      }
    }
  }
}


class petal {
  constructor(x, y, r) {
    this.x = x;
    this.y = y
    this.u = random(7, 9); //diameter of the petal on the long side
    this.p = random(5, 7); //diameter of the petal on the short side
    this.r = r //the rotation of the petal
  }

  show() {
    //draws the petals around the center
    push();
    translate(this.x, this.y)
    rotate(this.r)

    fill(petalColor)
    ellipse(0, 0, this.u, this.p)
    pop();
  }
}