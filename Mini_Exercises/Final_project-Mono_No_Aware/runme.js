//web-cam related variables
let video;
let vWidth = 32;
let vHeight = 24;

//variables for brightness "calculation"
let pixValues = [];
let brightness;

//variables for starting and storing the tree
let root;
let tree = [];
let stemEnd;

//variables for adjusting canvas size and positioning on the screen
let startWidth;
let currentWidth;
let startHeight;
let currentHeight;
let yOffset;
let xOffset;

//variables related to monitoring the health of a tree and displying the death message if it dies.
let treeHealth = 100;
let alive = true;
let deathFrame;
let growthFactor = 5;
let growthCounter = 0;

//variables for the flowers
let flowers = []
let fallingFlowers = []
let flowerBud;
let flowerBudColor;
let petalColor;

//the variable for the color of the earth in the pot
let earth = color = "#debf9e";

//the variable that holds the battery level
let batLevel;

function setup() {
  frameRate(60)
  createCanvas(windowWidth, windowHeight)
  colorMode(HSB, 360, 100, 100, 100)
  angleMode(DEGREES);
  textAlign(CENTER, CENTER)
  noStroke();
  pixelDensity(1);

  //establishes the starting values of the width of the canvas.
  startWidth = width;
  currentWidth = width;
  startHeight = height;
  currentHeight = height;
  yOffset = currentHeight - startHeight;
  xOffset = (currentWidth - startWidth) / 2;

  //sets the flower colors
  flowerBudColor = color(345, 53, 85, 90);
  petalColor = color(342, 25, 85, 85);

  //start the tree
  root = createVector(width / 2, height - 140);
  stemEnd = createVector(width / 2, height - 280);
  tree.push(new branch(root, stemEnd));

  //setup camera
  video = createCapture(VIDEO);
  video.size(vWidth, vHeight);
  video.hide();

  //runs the control loop every 3 seconds
  setInterval(controlLoop, 3000)
}

//resizes the canvas and makes sure the tree moves with it.
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
  currentWidth = width;
  currentHeight = height;
  xOffset = (currentWidth - startWidth) / 2;
  yOffset = currentHeight - startHeight
}

//setup battery monitor
navigator.getBattery().then(function(battery) {
  batLevel = battery.level;
  battery.onlevelchange = function() {
    batLevel = battery.level;
  };
});

//sets the earth color to reflect the batterylevel
function earthColor() {
  if (batLevel < 0.3) {
    earth = color = "#debf9e"
  } else if (batLevel >= 0.3 && batLevel <= 0.7) {
    earth = color = "#6a4125"
  } else if (batLevel > 0.7) {
    earth = color = "#291609"
  }
}

//increments or decrements the health of the tree
function calcHealth() {
  //if it has enough light and battery it's health increases if not it decreeses
  if (batLevel >= 0.3 && batLevel <= 0.7 && brightness > 100) {
    treeHealth += 2
  } else {
    treeHealth -= 2
  }
  treeHealth = constrain(treeHealth, 0, 100)
}

//this updates the status of the tree according to its health
function controlLoop() {
  if (alive) {
    calcHealth();

    //if the tree is at max health check if there is enough room to grow.
    if (treeHealth == 100) {
      //checks if all of the branches are inside canvas
      for (let i = 0; i < tree.length; i++) {
        if (tree[i].end.x + xOffset < 0 || tree[i].end.x + xOffset > width || tree[i].end.y + yOffset < 0 || tree[i].end.y + yOffset > height) {
          i = tree.length
          //if the tree isn't outside the canvas increment the growthCounter and make it grow slowly.
        } else if (i == tree.length - 1) {
          growthCounter += 1;
          if (growthCounter % growthFactor == 0) {
            grow()
          }
        }
      }
      // if the tree is at minimum health it will start droping flowers
    } else if (treeHealth == 0) {
      if (flowers.length > 0) {
        let index = floor(random(flowers.length))
        fallingFlowers.push(flowers[index]);
        flowers.splice(index, 1);
        //if the tree has dropped all of it's flowers the tree will die.
      } else if (flowers.length == 0 && fallingFlowers.length > 0) {
        alive = false;
        deathFrame = frameCount;
      }
    }
  }
}

//gets the average brightness of the camera feed and stores it in brightness
function getBrightnes() {
  pixValues = [];
  brightness = 0;

  //reads the red green and blue value of each pixel in the video feed
  video.loadPixels()
  for (let y = 0; y < vHeight; y++) {
    for (let x = 0; x < vWidth; x++) {
      let index = (x + y * vWidth) * 4;
      let r = video._pInst.pixels[index + 0];
      let g = video._pInst.pixels[index + 1];
      let b = video._pInst.pixels[index + 2];

      //stores the average of the red green and blue pixels. This essentially describes the brightness of the pixel.
      pixValues.push((r + g + b) / 3);
    }
  }

  //acumulates the brightness of each pixel into the brghtness variable temporarily.
  for (let i = 0; i < pixValues.length; i++) {
    brightness += pixValues[i]
  }

  //gets the average brightness of the whole video-feed and stores it in the brightness variable.
  brightness = floor(brightness / pixValues.length);
}

//grows all of the branches that are able to grow.
function grow() {
  for (let branch of tree) {
    branch.extendBranch();
  }
}

function draw() {
  background(80);
  fill(255);
  textSize(30);

  //this aproximates the brightness of the room depending on the average brightness of the camerafeed
  getBrightnes();

  //this sets the backgroundcolor acording to the brightness of the camerafeed if the camera is live.
  if (brightness) {
    background(200, 80, brightness);
  } else {
    background(0)
  }

  //updates the earth color depending on the batterylevel
  earthColor();

  //table
  push();
  fill(color = "#885b3a");
  rect(0, height - 100, width, 130);
  pop();

  //pot
  push();
  rectMode(CENTER);

  //bottom part of the pot
  fill(color = "#b68960");
  ellipse(width / 2, height - 90, 250, 40);
  rect(width / 2, height - 115, 250, 50);

  //top part of the pot
  fill(color = "#a9754e");
  ellipse(width / 2, height - 120, 270, 40);
  rect(width / 2, height - 130, 270, 25);
  ellipse(width / 2, height - 140, 270, 40);

  //earth in the pot
  fill(color = "#4a2a1b");
  ellipse(width / 2, height - 140, 250, 30);
  fill(earth);
  ellipse(width / 2, height - 138, 250, 30);
  pop();

  //displays the branches of the tree
  for (let i = 0; i < tree.length; i++) {
    tree[i].show();
  }

  //displays the flowers
  for (let i = 0; i < flowers.length; i++) {
    flowers[i].show();
  }

  //updates the position of the falling flowers and displays them on the screen
  for (let each of fallingFlowers) {
    each.show();
    each.fall();
  }

  //if the tree has died it writes "Your tree has passedd on the screen"
  if (!alive) {
    let opacity = map(frameCount - deathFrame, 0, 250, 0, 95)
    console.log(opacity)
    push();
    textAlign(CENTER, CENTER);
    fill(342, 25, 100, opacity);
    textSize(50);
    text("Your tree has passed.", width / 2, height / 2)
    pop();

    if (opacity > 100) {
      noLoop();
    }
  }
}