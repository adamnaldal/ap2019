//variables for storing and finding the bible quotes
let passage;
let book;
let chapter;

//variables for storing shower thoughts
let showerThoughts;
let thought;
let thoughtSelector = true;
let thoughtNumber;
let showerThoughtsHot;
let showerThoughtsTop;
let buttonPressed = 0;

//the variable for the button
let jambalam;

//variables related to selecting and modifying the bible quotes
let adressals = ['said unto them, ', 'saying, ',
  'Lord said, ', 'said unto him, ', 'say unto you, ', 'said he unto them, '
]
let newQuotes = [];
let displayQuote;

//variables related to style and layout
let holyFont;
let bgImg;
let topLeft = {};

function preload() {
  //The api for the bible quotes. This is weird way of getting information from the bible api but it was what they recommended on the api site and nothing else worked.
  jQuery.ajax({
    url: 'https://getbible.net/json?',
    dataType: 'jsonp',
    data: 'passage=Luke',
    jsonp: 'getbible',
    success: function(json) {
      // set text direction
      if (json.direction == 'RTL') {
        var direction = 'rtl';
      } else {
        var direction = 'ltr';
      }
      // check response type
      if (json.type == 'verse') {
        passage = json;
      } else if (json.type == 'chapter') {
        passage = json;
      } else if (json.type == 'book') {
        passage = json;
      }
    },
    error: function() {
      console.log("ERROR : no scripture was returned")
    },
  });

  //the shower thoughts that we get from reddit
  showerThoughtsTop = loadJSON("https://www.reddit.com/r/Showerthoughts/top/.json?limit=200", "jsonP")
  showerThoughtsHot = loadJSON("https://www.reddit.com/r/Showerthoughts/hot/.json?limit=200", "jsonP")

  //the font we use
  holyFont = loadFont('BLKCHCRY.TTF');

  //the background image
  bgImg = loadImage('bread.png')
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  topLeft.x = width / 2 + 40
  topLeft.y = height / 2 - 280

  //this is the button
  jambalam = createButton("New Verse");
  jambalam.addClass('button')
  jambalam.mousePressed(newVerse)
  jambalam.position(topLeft.x + 10, topLeft.y + 480)
  jambalam.class('jesuschrois')

  //here we create the first verse
  newVerse()
}

function draw() {
  background(255)

  //text color
  fill("#220b08")

  //this is the image of the book
  push();
  image(bgImg, width / 2 - (16 * 70) / 2, height / 2 - (9 * 70) / 2, 16 * 70, 9 * 70)
  pop();

  //the headline
  push()
  textSize(38);
  textAlign(LEFT, TOP)
  textFont(holyFont)
  text("Gospel of Luke; First draft", topLeft.x, topLeft.y);
  pop()

  //the main text
  push();
  textSize(24);
  textFont(holyFont);
  text(displayQuote.text, topLeft.x, topLeft.y + 70, 400, 600);
  pop();

  //the reference in the bottom right
  push();
  textSize(16);
  textFont(holyFont);
  textAlign(RIGHT, TOP);
  text(displayQuote.reference, topLeft.x + 400, topLeft.y + 480);
  pop();
}


function newVerse() {
  buttonPressed += 1;
  newQuotes = [];
  try {
    //here we choose first a random book number from the gospel of Luke and a random showerThoughts from the 100 we got from reddit

    book = random(Object.keys(passage.book));

    thoughtSelector = !thoughtSelector
    thoughtNumber = floor(random(1, 100));
    if (thoughtSelector) {
      thought = showerThoughtsHot.data.children[thoughtNumber].data.title
      console.log(buttonPressed, thoughtSelector, thoughtNumber, thought);
    } else {
      thought = showerThoughtsTop.data.children[thoughtNumber].data.title
      console.log(buttonPressed, thoughtSelector, thoughtNumber, thought);
    }
    console.log(buttonPressed, thoughtSelector, thoughtNumber, thought);

    //here we go through all of the verses (also called chapters here) in the chosen book
    for (let i = 0; i < Object.keys(passage.book[book].chapter).length; i++) {

      //for each of them we get the text of the verse
      let rawText;
      rawText = passage.book[book].chapter[i + 1].verse

      //we then se if it contains any of the adressals to figure out if anyone says something preceded by one of the sentences.
      for (let adress of adressals) {
        if (rawText.includes(adress) && i > 1) {

          //if it does we then get the verse preceding the one with someone speaking and the verse with someone speaking and replaces what they say with a showerThought. All of these modified quotes are put into an array called newQuotes along with their reference numbers.
          newQuotes.push({
            text: passage.book[book].chapter[i].verse.slice(0, passage.book[book].chapter[i].verse.length - 2) + "\n\n" + rawText.split(adress)[0] + adress + thought,
            reference: "Luke " + book + ":" + passage.book[book].chapter[i].verse_nr + "-" + passage.book[book].chapter[i + 1].verse_nr
          });
        }
      }
    }

    console.log(newQuotes)
    //lastly we chose one of the modified quotes to display
    displayQuote = random(newQuotes);
  } catch (error) {
    console.error("what" + error);
  }
}