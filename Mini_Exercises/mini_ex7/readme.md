## Mini exercise 7 - Lightning and rainbows
![Lightning and rainbows](Screenshot.gif)

[link](https://gl.githack.com/adamnaldal/ap2019/raw/master/Mini_Exercises/mini_ex7/index.html)

### Description
This program is a self resetting lightning generator. It generates a new lightning every time it runs while slowly looping through the whole colour spectrum. Each time it branches it creates a little pop of colour that looks a bit like fireworks.

The three rules i wanted the lightning to follow as it is being generated was:
1. Always move down but in a line shaped by perlin-noise.
2. The bigger the lightning the greater the chance that it will split into two smaller in any given frame.
3. When it splits it should "pop" creating a rainbow coloured sparkle.

### Putting it into perspective
In all of my other mini exercises the visual presentation has been secondary to the conceptual idea of the program. In this exercise i wanted to use the generative approach to create something where the visually aesthetic aspect is the primary focus of the program. As a result, it isn't very conceptually advanced. The concept was essentially to take two of the most visually stunning phenomenon of the sky: lightning and rainbows and unify them in a single visual experience.
