let lightnings = [];
let checkSum;
let startWeight;
let colors = 220;
let explosions = [];

function setup() {
  frameRate(30);
  colorMode(HSB, 360, 100, 100, 100);
  angleMode(DEGREES);
  createCanvas(windowWidth, windowHeight);

  background(260, 40, 10);
  background(240, 5, 100, 50);

  startWeight = floor(random(3, 5));

  //this creates the initial start-lightning
  lightnings.push(new lightning(width / 2 + random(-(width / 8), width / 8), 0, startWeight, colors));
}

function draw() {
  background(260, 40, 10, 25);

  checkSum = 0;
  for (let each of lightnings) {
    if (each.active) {
      each.grow();
      each.control();

      //this sets the cheksum to 1 if any of the lightning are still active (i tried doing this with booleans but for some reason that didn't work???)
      checkSum = 1;
    }
    each.show();
  }

  //this annimates each of the explosions
  for (let each of explosions) {
    each.explode()
  }

  //if none of the lightning are still active this restarts the program.
  if (checkSum == 0) {
    restart();
  }
}

function restart() {
  background(colors, 20, 100, 60) //this makes the screen flash brightly in the color of the lightning being made.

  //this clears all the lightning and explosions currently in the arrays
  lightnings = [];
  explosions = [];
  //this creates a new start-lightning
  lightnings.push(new lightning(width / 2 + random(-(width / 8), width / 8), 0, startWeight));
}

class lightning {
  constructor(startX, startY, weight) {
    this.startX = startX;
    this.startY = startY;
    this.direction = random(-30, 30);
    //this is the array that contains each of the points along each of the lines.
    this.points = [
      [this.startX, this.startY]
    ];
    this.weight = weight;
    this.active = true;
    this.willSplit;
    this.seed = random(0, 50000); //this seed is used to give ach lightning a unique spot in the noise-space to draw it's shape from.
    this.counter = 0;

    //this little group controlls the colors making sure they change gradually with each new lightning.
    if (colors > 360) {
      colors = 0;
    }
    this.color = colors;
    if (this.startY < height) {
      colors += 1;
    }
  }

  //this draws a line between the verteces in the array, essentially displaying the lightning.
  show() {
    noFill();
    stroke(this.color, 80, 98);
    strokeWeight(this.weight);
    beginShape();
    for (let each of this.points) {
      vertex(each[0], each[1])
    }
    endShape();
  }

  grow() {
    if (this.active) {
      this.counter += 1;

      //here we push new points to the array of points, the new point is always 1/22 of the windows height further down than the last.
      //the X value of the new point is pulled from a noise function to give the lightning a random but coherent path. It also adds in the direction of the particular lightning which is what makes some lightning go very diagonally and others not so much.
      this.points.push([this.points[this.points.length - 1][0] + map(noise(this.seed + this.counter), 0, 1, -30, 30) + this.direction, this.points[this.points.length - 1][1] + height / 22])
    }
  }

  control() {
    //this makes the lightning inactive after it has grown past double the canvas' height. The reason it is double is to create the pause after the lightning has reached the bottom until it resets.
    if (this.points[this.points.length - 1][1] > height * 2) {
      this.active = false;
    }

    //this generates a random number and if the current lightning is wider than 1 and the random number is high enough it will split into two smaller lightning and become inactive.
    //If the current lightning is thinner than 1 it won't split but just stop and become inactive.
    //the wider the lightning the more likely it will split.
    this.willSplit = random();
    if (this.willSplit > 1 - map(this.weight, 1, 5, 0.02, 0.3)) {
      if (this.weight > 1) {
        for (let i = 0; i < 2; i++) {
          lightnings.push(new lightning(this.points[this.points.length - 1][0], this.points[this.points.length - 1][1], this.weight - 1));
        }
        this.active = false;

        //this creates a little visual pop when the lightning splits.
        explosions.push(new fireworks(this.points[this.points.length - 1][0], this.points[this.points.length - 1][1], this.weight - 1))
      } else {
        this.active = false;
      }
    }
  }

}

// this is the class for the pops that happen when the lightning splits.
class fireworks {
  constructor(x, y) {
    this.cX = x;
    this.cY = y;
    this.r = 0;
    this.maxDist = random(18, 34);
    this.size = 2;
    this.spread = 5;
    this.fade;
    this.startColor = floor(random(360));
  }

  explode() {
    this.fade = this.maxDist - this.r;
    this.r += this.fade * 0.15; //this makes the circle of dots expand fast at first and then slower.

    push();
    translate(this.cX, this.cY)
    noStroke();

    //this makes the color of the pop match that of the lightning and makes it fade as it grows.
    // fill(colors, 60, 100, map(this.fade, 0, this.maxDist, 0, 100));

    //this draws the circles of dotts with some randomness to give it more of an explosive look
    for (let i = 0; i < 360; i += this.spread) {
      if (this.startColor > (360 - this.spread)) {
        this.startColor = 0;
      }
      fill(this.startColor, 80, 100, map(this.fade, 0, this.maxDist, 0, 100));
      this.startColor += this.spread

      rotate(i);
      ellipse(this.r + random(this.maxDist / 3), 0, this.size)
    }
    pop();
  }
}