var speedBoost; // this will control how much faster the throbber will rotate depending on the slider position
var circleCount = 7; //determines the number of circles that make up the throbber
var rSpeed; //determines how fast the throbber will rotate
var img;

function preload() {
  img = loadImage('assets/breaking-bad-1.png');
}

function setup() {
  createCanvas(1263 * 0.6, 695 * 0.6);
  colorMode(HSB, 360, 100, 100, 100);
  speed = new slider(width / 2, height - 95); //this creates the slider
  textAlign(CENTER)
  angleMode(DEGREES);
  frameRate(30);
}

//this function defines the jitter based on a single input parameter
function jitter(s) {
  var jitter = noise(frameCount + (s * 1000) * 0.05 * speed.slider.value()) * map(speed.slider.value(), 1, 100, 0, 15); //defines how fast the jitters go and the amplitude of the jitters
  return jitter;
};

function draw() {
  push();
  scale(0.6);
  image(img, 0, 0);
  pop(); //this draws the screenshot of the streaming service i nthe background

  push();
  noStroke();
  fill(0, 0, 0, 40);
  rect(0, 0, width, height);
  pop(); //this puts a semi-transparent black overlay over the screenshot to divert focus to the throbber and text

  speed.show(); //this displays the slider

  speedBoost = map(speed.slider.value(), 1, 100, 1, 8); //maps the number from the slider to a usable number range

  rSpeed = frameCount * speedBoost * 1.5; //sets the speed at which the throbber rotates

  noiseDetail(map(speed.slider.value(), 1, 100, 5, 2), map(speed.slider.value(), 1, 100, 0.25, 1.2)); //decides how violent the circles jitter depending on the slider value

  //__text__
  push();
  fill(0, 0, 70, 80);
  textSize(20);
  text("Turbo-load: ", width / 2 - 20, height - 100); //draws the turboload text
  text(floor(map(speed.slider.value(), 1, 100, 0, 100)) + "*", width / 2 + 55, height - 100); // draws the number value of turbo-load
  push();
  textSize(12)
  text("®", width / 2 + (textWidth("Turbo-Load")) / 2, height - 110); //draws the ® sybol at the right size and position
  pop();
  textSize(10);
  fill(0, 0, 60);
  text("*This will not load your content any faster, but it will give you the illusion of control.", width / 2, height - 65); //this draws the disclaimer at the bottom
  pop();

  //here we draw and animate the throbber
  push();
  fill(0, 0, 70, 80);
  noStroke()
  translate(width / 2 - 5 + random(0, jitter(3) * 1.), height / 2 - 20 + random(0, jitter(13) * 1.35)); //sets the center of the throbber
  rotate(rSpeed); //makes the throbber go around
  for (var i = 0; i < circleCount; i++) {
    rotate(360 / circleCount); //draws circles around center
    ellipse(circleCount * PI * 2.3 + jitter(i + 20) + speedBoost * 1.5, 0, 30 + jitter(i + 300) / 2, 30 + jitter(i + 500)) / 2; // draws the ellipses evenly spaced and applies jitter to their distance from the center, size and shape
  }
  pop();
}

//this just describes the slider
class slider {
  constructor(x_, y_) {
    this.x = x_;
    this.y = y_;
    this.slider = createSlider(1, 100, 0);
  }
  show() {
    this.slider.position(this.x - 100, this.y);
    this.slider.style('width', '200px');
  }
}