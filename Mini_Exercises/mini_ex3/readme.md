## Mini exercise 3
![Load Faster!](Screenshot.gif)

[link](https://cdn.staticaly.com/gl/adamnaldal/ap2019/raw/master/Mini_Exercises/mini_ex3/index.html)

### Description
The programme overlays a throbber and a slider with some text on top of a typical video-streaming service. The slider makes the throbber spin faster but it also makes the throbber behave irregularly, making it seem stressed or unstable. To do this i tied the slider to a speedBoost variable to make it spin faster and a noise function that i applied to the relative position of the circles and the position of the throbber to make them fluctuate.

### Putting it into perspective
When reading about throbbers and micro temporal analysis i found it interesting how throbbers don't reflect the actual buffering process or data transfer. I wanted to play with this idea of how the throbber is mostly meant to comfort people while they wait and reassure them that "it's going to be all right, your computer is working". The idea was to create a throbber that would give the user the ability to make it seem like the computer is working harder or speeding up the data transmission but in reality is still just as far removed from the actual processes of the computer as a regular throbber.

This illusion of control is something that is also present in other interactions with digital media. When playing an exiting game for example, the player might hit the buttons extra hard to perform an action in the game with more power and effect, or lean their body to the side to steer their character better, as in a racing game e.g. I think this sort of interaction highlights some of the fundamental differences between how humans and computers work and how we humans tend to project human qualities on to the interfaces we use to interact with the computers.
