var cam;
var jumpHeight;
var jitter;
var speedBoost;

function setup(){
  createCanvas(600,500,WEBGL);
  colorMode(HSB);
  frameRate(60);
  angleMode(DEGREES);
  perspective();
  cam = createCamera();
  cam.setPosition(0,-400,300);
  cam.lookAt(0, -100, 0);
}

function draw(){
  background(0,0,10);
  noStroke();
   jitter = noise(frameCount)*0;
   speedBoost = 10;

  sphere(20);

  if (floor(cos(frameCount)*100) > 99){
    console.log(frameCount);
  }
    for (var i =0; i < 9; i++){
      rotateY(40+jitter/20);//orients the coins in a circle
      translate(0,0,0)
      jumpHeight = sq(sin(frameCount-(i*4))*10)*-1;//decides how high the coins jump

      push();
      translate(100+jitter, jumpHeight+jitter, cos(frameCount)*100+jitter);//translates the coins outwards and inwards

      rotateX((cos(frameCount)*720+jitter)*-1); //makes the coins flip
      cylinder(30,5,100,100);
      pop();
    }

  pointLight(280,50,100,-80,-40,100);
  ambientLight(250,0,50);
  ambientMaterial(240,20,60);


  //console.log(cos(frameCount))
}
