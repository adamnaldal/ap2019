## Your Digital Identity

![image](Screenshot.png)


[link](https://gl.githack.com/adamnaldal/ap2019/raw/master/Mini_Exercises/mini_ex4/index.html)

In this sketch i capture text input from the user and store it in an array. The user is asked some, more or less personal questions and when all the questions have been asked the program uses the speech synthesis library by R. Luke DuBois to "assume" the identity of the user by incorporating the users input into a first person paragraph, informing the user verbally that this voice is the new them. It is meant to comment on the harvesting and selling of personal data by companies like Google and Facebook by making the transferal of ownership of user data to these companies explicit and transparent where it is usually hidden behind pages of legal jargon in the terms and conditions of service.

The programme was inspires by the text "The like economy: Social buttons and the data-intensive web" by Carolin Gerlitz and and Anne Helmond (2013) and particularly their observations on the "like economy" and how users are essentially providing free labor that enables companies to track and target these same users with advertisements. I also make a secondary comment on the inhibited range of expression allowed by these platforms, in particular in regards to gender by not allowing the user to input anything but "man" or "woman" when asked about their gender. In doing this i hope to point to the broader idea that the data that represents individual users on the web is not accurately descriptive of the actual people it is meant to represent. 


### References
Gerlitz, C., & Helmond, A. (2013). The like economy: Social buttons and the data-intensive web. New Media & Society, 15(8), 1348–1365. https://doi.org/10.1177/1461444812472322
