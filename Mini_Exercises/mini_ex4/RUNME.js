//For this sketch i use the speech syntesis and speech recognition library by R. Luke DuBois (dubois@nyu.edu)  for p5.js.


let input; //declares the variable where the input field is stored.
let error = false; //controls if the error box is displayed
let soliloquy;

let qaIndex = 0; //a global variable that controls which question is being asked and where to find the answers when th  computer talks
let qs = ["Whats your name?", "Are you a MAN or a WOMAN?", "How old are you?", "List some things you like to do.", "Describe yourself with one word", "Thank you, that was all. Please give me a minute.", "Identity assumed."] //the array containing the questions
let as = []; //the array where the answers are stored

let comVoice = new p5.Speech(); //a variable to be called when i wan't the computer to speak.
let header = "Please, tell me a little bit about yourself."; //Stores what is supposed to be stored in the header
let secondStage = false; //a variable that controls whether all questions have been answered and sets of the second phase of the program


function setup() {
  colorMode(HSB, 360, 100, 100, 100);
  frameRate(30);
  input = createInput(); //initialises the input

  comVoice.setPitch(0.45);
  comVoice.listVoices();
  comVoice.setRate(0.8); //these adjust the sound of the voice synthesis
}

function draw() {
  createCanvas(windowWidth, windowHeight);
  background(221, 64, 60);

  push();
  input.position(width / 2, height / 2);
  input.center('horizontal');
  input.size('300');
  input.changed(inputUpdate); //this runs the inputUpdate (line 58) every time the user presses enter in the input box
  pop(); //positions and resizes the input box

  textAlign(CENTER, BOTTOM);
  rectMode(CENTER);
  fill(0, 0, 95);
  push();
  textSize(28);
  textFont('Helvetica Neue');
  text(header, width / 2, height / 2 - 160, width, 100)
  pop(); //displays the header

  push();
  textSize(16);
  text(qs[qaIndex], width / 2, height / 2 - 50);
  pop(); //displays the questions

  if (error == true) { //this block controls the error message
    push();
    textSize(16);
    textStyle(BOLD);

    push();
    stroke(0, 0, 0, 20)
    rect(width / 2, height / 2 + 46, textWidth("  ERROR: not a valid gender.  "), 22);
    pop(); //draws the box

    fill(0, 90, 80);
    text("ERROR: not a valid gender.", width / 2, height / 2 + 55) //draws the text
    pop();
  }
}

function inputUpdate() {
  if (qaIndex == 1) { //checks if the question is "are you a man or a woman".
    console.log("step1")
    if (input.value().toLowerCase() == "man" || input.value().toLowerCase() == "woman") { //if the answer fits the strict gender binary
      console.log("step2if")
      error = false;
      continueToNextQuestion();
    } else {
      error = true;
      console.log("step2else")
      input.value(''); //this clears the input box.
    }
  } else { //if it is any other question than "are you a man or a woman".
    continueToNextQuestion();
  }
}

function assumeIdentity() {
  soliloquy = "Thank you for your cooperation. My name is " + as[0] + " 2 point O. I am a " + as[2] + " year old " + as[4] + " " + as[1] + ". When i'm not talking to you, i like to " + as[3] + ". I am the new you. The digital you. The you, that is bought and sold to advertisers on the internet." //the thing the computer says.

  input.hide(); //hides the input box
  header = "Vocalizing!" //changes the header
  qaIndex = 6; //writes the text "identity assumed" stored at the end of the questions array.
  comVoice.speak(soliloquy); //fires the speach syntehesis - makes the computer talk
}

function continueToNextQuestion() {
  as.push(input.value()); //this adds whatever the user inputed at the end of the answers array.
  input.value(''); //this clears the input box.

  if (qaIndex < 5) { //checks if all the questions have been answered.
    qaIndex += 1;
  }
  if (qaIndex == 5 && secondStage == false) { //if all the questions have been answered enter the sexond stage.
    secondStage = true;
    header = "Creating person..." //changes the header
    setTimeout(assumeIdentity, 4000); //fires the assumeIdentity function (line 76) after 4 seconds
  }
}
