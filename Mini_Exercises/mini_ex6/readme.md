## Mini exercise 6
![Antarctica](Screenshot.png)

[link](https://cdn.staticaly.com/gl/adamnaldal/ap2019/raw/master/Mini_Exercises/mini_ex6/index.html)

### Description
This game is inspired by other similar web-resource-management and civilisation-building games. The goal is to survive and grow your little colony of penguins by managing what each penguin is doing and buying upgrades and different structures. I personally find these games really addictive and entertaining to play and interesting to try and reverse engineer. For the same reason i bit of a bit more than i could chew with this mini-exercise: i wanted to ad a bit more complexity to this game but as i have already used way more time on this than i should this is how it is.

I also think that this program could be a lot more object-oriented but i just didn't have the technical knowledge of how to do this so it is really more of a functional or procedural program with a hint of objects thrown in.

### The idea
The game is intentionally challenging and slow as it is meant to run in the background and be about preparation and planning. There is also a lot of different aspects of the game that is hidden or obscured for the user. Different numbers are represented abstractly as bars instead of numbers and what factors go into calculating the different numbers are hidden, e.g. hatching a live egg is a lot harder in the winter than in the summer and how long it takes for an egg to hatch is also not obvious.
