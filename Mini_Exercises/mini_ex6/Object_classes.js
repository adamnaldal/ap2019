class snowFlake {
  constructor() {
    this.diameter = random(5, 15);
    this.xPos = random(width + 20);
    this.yPos = random(-50, height)
    this.xSpeed = 3;
    this.ySpeed = 0.5;
  }

  show() {
    push();
    fill(0, 0, 100);
    noStroke();
    ellipse(this.xPos, this.yPos, this.diameter);
    pop();
  }

  update(xMOD, yMOD) {
    this.xPos -= this.xSpeed * xMOD * 0.7 + this.diameter / 10;
    this.yPos += this.ySpeed * 0.5 + this.diameter / 10;

    if (this.xPos < -15 || this.yPos > height + 15) {
      this.xPos = width + random(10, 50);
      this.yPos = random(-280, height - 60);
    }
  }
}

let names = ['Tails', 'Steel', 'Cannon', 'Fang', 'Flow', 'Scratch', 'Floyd Kidd', 'Cooper Holmes', 'Jason Living', 'Daryl Swift', 'Jewel', 'Brawn', 'Life', 'Kid', 'Face', 'Skyler Banks', 'Danni Nash', 'Skye West', 'Riley Powell', 'Spirit', 'Brick', 'Axis', 'Blue', 'Stitch', 'Willy Powers', 'Riley Living', 'Rene George', 'Leslie Alexander', 'Angel', 'Light', 'Luck', 'Judge', 'Flock', 'Kai Castle', 'Maddox Cole', 'Carmen Fay', 'Jo Fawn', 'Bird', 'Sugar', 'Boar', 'Gold', 'Escape', 'Reed', 'Drake', 'Blair Turner', 'Danni Hill', 'Mell Lover', 'Drum Stick', 'Grete', 'Arnold', 'Kenned', 'Makker Ronni', 'Slim Jim Lenard', 'Pappi', 'Rene', 'Silver'];

const occupations = ['fishing', 'parenting', 'mining'];

class penguin {
  constructor() {
    this.nameindex = floor(random(0, names.length + 1));
    this.name = names[this.nameindex];
    this.fishing = random(1, 3);
    this.parenting = random(1, 3);
    this.mining = random(1, 3);
    this.occupation = random(occupations);
    this.hp = 2000;

    names.splice(this.nameindex, 1);
  }

  show() {
    push(); //all text
    textAlign(LEFT, TOP);

    // name of the penguin
    push();
    textStyle(BOLD);
    textSize(16);
    text(this.name, colmn3, topMargin);
    pop();

    //penguins hp:
    textSize(14);
    text("HP:", colmn3, topMargin + 20);
    rect(colmn3 + 70, topMargin + 22, 70, 12);
    push();
    fill(0, 0, 75);
    rect(colmn3 + 70, topMargin + 22, map(this.hp, 0, 2000, 0, 70), 12)
    pop();

    // stats of the penguin
    textSize(14);
    text("Stats:", colmn3, topMargin + 40);

    //fishing bar
    rect(colmn3 + 70, topMargin + 60, 70, 12);
    push();
    fill(0, 0, 75);
    rect(colmn3 + 70, topMargin + 60, map(this.fishing, 1, 3, 0, 70), 12)
    pop();

    //parenting bar
    rect(colmn3 + 70, topMargin + 78, 70, 12);
    push();
    fill(0, 0, 75);
    rect(colmn3 + 70, topMargin + 78, map(this.parenting, 1, 3, 0, 70), 12)
    pop();

    //mining bar
    rect(colmn3 + 70, topMargin + 96, 70, 12);
    push();
    fill(0, 0, 75);
    rect(colmn3 + 70, topMargin + 96, map(this.mining, 1, 3, 0, 70), 12)
    pop();

    //current occupation:
    push();
    textSize(14);
    text("Occupation: " + this.occupation, colmn3, topMargin + 114);
    pop();
    pop();

  }

  updateHP() {
    //this controls the hp of the penguins
    if (herring >= 0 && this.hp <= 2000) {
      this.hp += 0.3;
    } else if (herring < 0) {
      this.hp -= 1;
    }
  }
}

class egg {
  constructor() {
    this.hatched = false;
    this.timer = 2000;
    this.age = 0;
    this.price = eggBasePrice + eggs.length * 0.5;
  }
  update(survival) {
    this.price = eggBasePrice + eggs.length * 0.5;
    this.age += 1 * seasonalBoost * 0.8;
    if (this.age >= this.timer) {
      this.survival = survival;
      if (this.survival > 0.50) {
        makePenguin();
      }
      this.hatched = true;
    }
  }
}

class structure {
  constructor(_priceH, _priceI) {
    this.name;
    this.type;
    this.amount;
    this.priceHerring = _priceH;
    this.priceIce = _priceI;
  }
  get _priceH() {
    return this.priceHerring;
  }

  set _priceH(newPrice) {
    this.priceHerring;
  }

  get _priceI() {
    return this.priceIce;
  }

  set _priceI(newPrice) {
    this.priceIce;
  }
}

class basicIgloo extends structure {
  constructor(_priceH, _priceI) {
    super(_priceH, _priceI)
    this.name = "Basic Igloo";
    this.type = "penguinCapacity";
    this.amount = 2;
  }
}