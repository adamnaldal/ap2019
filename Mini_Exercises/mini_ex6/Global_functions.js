function makePenguin() {
  penguins.push(new penguin);
  //i had an issue where some penguins names would come out as undefined and trying to select an undefined penguin would break the penguinSelector, this checks for undefined penguins and retries until it succeds
  if (penguins[penguins.length - 1].name) {
    penguinSelector.option(penguins[penguins.length - 1].name);
    eggBasePrice = eggBasePrice * 1.1;
  } else {
    penguins.splice(penguins.length - 1, 1);
    makePenguin();
  }
}

function makeEgg() {
  if (eggs.length) {
    if (eggs[eggs.length - 1].price < herring) {
      herring -= eggs[eggs.length - 1].price;
      eggs.push(new egg);
    }
  } else if (eggBasePrice < herring) {
    herring -= eggBasePrice;
    eggs.push(new egg)
    eggsHaveBeenLaid = true;
  }
}

function makeBasicIgloo() {
  if (basicIglooPrice[0] < herring && basicIglooPrice[1] < ice) {
    herring -= basicIglooPrice[0];
    ice -= basicIglooPrice[1];
    penguinCapacity += 2;
    basicIgloos.push(new basicIgloo);
    for (let i = 0; i < basicIglooPrice.length; i++) {
      basicIglooPrice[i] = basicIglooPrice[i] * 1.2;
      console.log("up")
    }
  }
}

function changeSelectedPenguin() {
  for (let i = 0; i < penguins.length; i++) {
    if (penguinSelector.value() == penguins[i].name) {
      chosenPenguin = penguins[i]
      occupationSelector.remove();
      createOccupationSelector();
    }
  }
}

function createOccupationSelector() {
  occupationSelector = createRadio();
  occupationSelector.position(colmn3, topMargin + 54);
  occupationSelector.style('font-size', '11px')
  occupationSelector.style('font-family', 'Arial')
  occupationSelector.style('width', '68px')
  for (let each of occupations) {
    occupationSelector.option(each)
  }
  occupationSelector.changed(changePenguinsOccupation);
}

function changePenguinsOccupation() {
  chosenPenguin.occupation = occupationSelector.value();
}

function changeSeason() {
  if (season == 'Summer') {
    season = 'Winter';
    seasonalBoost = 1;
    bg.h = 220;
    bg.s = 4;
    bg.b = 94;
  } else {
    season = 'Summer';
    year += 1;
    seasonalBoost = 2;
    bg.h = 220;
    bg.s = 7;
    bg.b = 99;
  }
}