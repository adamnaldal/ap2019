function startScreen() {
  let tut = 0;
  this.draw = function() {
    background(0, 0, 94);

    push();
    fill(0, 0, 20);
    textAlign(CENTER, CENTER);
    textSize(20);

    push();
    textSize(16);
    if (tut < 5) {
      text("Tutorial: " + tut + "/4", width - 50, 15)
    }
    pop();
    if (tut == 0) {
      text("Welcome to penguin-land!", width / 2, 30);
      text("Click mouse to continue.", width / 2, 55);
    } else if (tut == 1) {
      text("Welcome to penguin-land!", width / 2, 30);
      text("Click mouse to continue.", width / 2, 55);
      image(tutorial, 5, 70, w - 10, h - 80);

      push();
      fill(0, 0, 0, 20);
      beginShape();
      vertex(5, 70);
      vertex(w - 5, 70)
      vertex(w - 5, h - 10)
      vertex(5, h - 10);
      beginContour();
      vertex(10, 75);
      vertex(10, 180);
      vertex(170, 180);
      vertex(170, 75);
      endContour();
      endShape();
      pop();

      push();
      strokeWeight(2);
      stroke(0, 100, 90);
      line(165, 89, 253, 89);
      line(90, 140, 90, 196);
      pop();

      push();
      textSize(14);
      textStyle(BOLD)
      textAlign(LEFT, CENTER)
      fill(0, 90, 90);
      text("This shows the season and year. \nWinter is harsher than summer.", 263, 89)
      text("This shows your available \nresources. Your penguins need \nherrings to survive, if it goes \nbelov zero they will starve.", 20, 230);
      pop();
    } else if (tut == 2) {
      text("Welcome to penguin-land!", width / 2, 30);
      text("Click mouse to continue.", width / 2, 55);
      image(tutorial, 5, 70, w - 10, h - 80);

      push();
      fill(0, 0, 0, 20);
      beginShape();
      vertex(5, 70);
      vertex(w - 5, 70)
      vertex(w - 5, h - 10)
      vertex(5, h - 10);
      beginContour();
      vertex(220, 100);
      vertex(220, 260);
      vertex(410, 260);
      vertex(410, 100);
      endContour();
      endShape();
      pop();

      push();
      strokeWeight(2);
      stroke(0, 100, 90);
      line(231, 130, 130, 165);
      line(234, 158, 140, 220);
      line(268, 240, 268, 300);
      pop();

      push();
      textSize(14);
      textStyle(BOLD)
      textAlign(LEFT, CENTER)
      fill(0, 90, 90);
      text("The number of \npenguins you have.", 20, 180);
      text("Here you can \nselect a penguin.", 40, 230);
      text("Here you can see how many \neggs you have and how much \nherring you need to lay a new one. \nYou also need to have spare room \nto lay eggs.", 190, 350)
      pop();
    } else if (tut == 3) {
      text("Welcome to penguin-land!", width / 2, 30);
      text("Click mouse to continue.", width / 2, 55);
      image(tutorial, 5, 70, w - 10, h - 80);

      push();
      fill(0, 0, 0, 20);
      beginShape();
      vertex(5, 70);
      vertex(w - 5, 70)
      vertex(w - 5, h - 10)
      vertex(5, h - 10);
      beginContour();
      vertex(410, 100);
      vertex(410, 260);
      vertex(580, 260);
      vertex(580, 100);
      endContour();
      endShape();
      pop();

      push();
      strokeWeight(2);
      stroke(0, 100, 90);
      line(405, 129, 180, 165);
      line(414, 150, 180, 210);
      line(415, 176, 300, 290);
      line(488, 254, 490, 300);
      pop();

      push();
      textSize(14);
      textStyle(BOLD)
      textAlign(LEFT, CENTER)
      fill(0, 90, 90);
      text("This area shows the \npenguin you have selected", 20, 175);
      push()
      stroke(0, 90, 90);
      strokeWeight(2);
      noFill();
      beginShape();
      vertex(410, 100);
      vertex(410, 260);
      vertex(580, 260);
      vertex(580, 100);
      endShape(CLOSE);
      pop();
      text("This is your penguins \nHP (health points). If it falls to \nzero that penguin dies.", 20, 230)
      text("This shows what your penguin is \ngood at. High fishing means that \npenguin catches more fish, high \nparenting makes it better at \nkeeping eggs alive and mining is \nused for getting ice for \nbuildings.", 170, 365);
      text("This shows what your \npenguin is doing now. \nYou can choose what \neach penguin is doing \nwith the check boxes.", 420, 350);

      pop();
    } else if (tut == 4) {
      text("Welcome to penguin-land!", width / 2, 30);
      text("Click mouse to continue.", width / 2, 55);
      image(tutorial, 5, 70, w - 10, h - 80);

      push();
      fill(0, 0, 0, 20);
      beginShape();
      vertex(5, 70);
      vertex(w - 5, 70)
      vertex(w - 5, h - 10)
      vertex(5, h - 10);
      beginContour();
      vertex(30, 280);
      vertex(30, 330);
      vertex(230, 330);
      vertex(230, 280);
      endContour();
      endShape();
      pop();

      push();
      strokeWeight(2);
      stroke(0, 100, 90);
      line(220, 294, 260, 294);
      line(51, 325, 51, 350);
      pop();

      push();
      textSize(14);
      textStyle(BOLD)
      textAlign(LEFT, CENTER)
      fill(0, 90, 90);
      text("This button makes a new igloo \nif you can afford it.", 270, 300)
      text("This shows the amount of herring \nand ice needed to buy a new igloo. \nYou need igloos to make room for \nmore penguins.", 40, 390)
      pop();
    } else if (tut == 5) {
      text("Grow your flock, and survive the winter.\n\nPress enter to begin.", width / 2, height / 2);
    }
    pop();

  } //draw ends here

  this.keyPressed = function() {
    if (keyCode == ENTER) {
      manage.showScene(gameScreen);
    }
  }

  this.mouseClicked = function() {
    if (tut < 5) {
      tut += 1;
    } else {
      manage.showScene(gameScreen);
    }
    console.log(mouseX, mouseY);
  }

}


/*
================================================
================================================
=                Main game code                =
================================================
================================================
*/
let bg = {
  h: 220,
  s: 5,
  b: 96
}
let snow = [];
let maxFlakes = 100;
let snowSpeed;

let season;
let seasonalBoost = 2;
let year = 0;

let penguins = [];
let penguinSelector;
let chosenPenguin;
let occupationSelector;
let penguinCapacity = 2;
let roomForPenguins;

let herring = 0;
let ice = 0;

let eggs = [];
let numberOfEggs = 0;
let eggBasePrice;
let eggbutton;
let eggsHaveBeenLaid = false;
let parentingBoost;
let survivalChance;

let basicIgloos = [];
let iglooButton;
let basicIglooPrice = [50, 100];

const topMargin = 50;
const boxMargin = 210;
const colmn1 = w * 0.05;
const colmn2 = w * 0.40;
const colmn3 = w * 0.70;


function gameScreen() {
  this.setup = function() {
    //here we create the snowflakes
    while (snow.length < maxFlakes) {
      snow.push(new snowFlake());
    }

    //sets the starting price of Eggs
    eggBasePrice = 68;

    //controls the seasons
    season = 'Summer'
    setInterval(changeSeason, 60000);

    //creates the egg button
    eggButton = createButton('+');
    eggButton.position(colmn2 + 70, topMargin + 76);
    eggButton.mousePressed(makeEgg);

    //creates the igloo button.
    basicIglooButton = createButton('+')
    basicIglooButton.position(colmn2 - 50, boxMargin)
    basicIglooButton.mousePressed(makeBasicIgloo)

    //this creates the penguin selector-dropdown menu
    penguinSelector = createSelect().position(colmn2, topMargin + 22);
    for (let each of penguins) {
      penguinSelector.option(each.name);
    }
    chosenPenguin = penguins[0];
    penguinSelector.changed(changeSelectedPenguin);
    penguinSelector.class('shit')

    //this creates the occupation radio used to change the occupation of different penguins. It is in a seperate function in RUNME.js because the original one is deleted and recreated everytime you change penguin.
    createOccupationSelector();

    // we start out with two penguins
    while (penguins.length < 2) {
      makePenguin();
    }

    chosenPenguin = penguins[0];
  }

  this.draw = function() {
    // -------------------------------------
    //    background, snow and mountains
    // -------------------------------------
    background(bg.h, bg.s, bg.b);

    push();
    strokeWeight(4);
    stroke(0, 0, 97);
    fill(0, 0, 99);
    curveTightness(-0.5);
    beginShape();
    curveVertex(-30, height + 40);
    curveVertex(-30, height + 40);
    curveVertex(-10, height - 40);
    curveVertex(50, height - 60);
    curveVertex(120, height - 20);
    curveVertex(210, height - 40);
    curveVertex(240, height - 20);
    curveVertex(230, height + 40);
    curveVertex(230, height + 40);
    endShape();
    beginShape();
    curveVertex(200, height + 10);
    curveVertex(200, height + 10);
    curveVertex(260, height - 80);
    curveVertex(320, height - 75);
    curveVertex(360, height - 35);
    curveVertex(400, height - 35);
    curveVertex(440, height - 60);
    curveVertex(500, height - 60);
    curveVertex(540, height - 50);
    curveVertex(580, height + 50);
    curveVertex(580, height + 50);
    endShape()
    beginShape();
    curveVertex(470, height + 10);
    curveVertex(470, height + 10);
    curveVertex(520, height - 10);
    curveVertex(550, height - 50);
    curveVertex(580, height - 70);
    curveVertex(width + 20, height - 80);
    curveVertex(width + 30, height);
    curveVertex(width + 30, height);
    endShape();
    pop();

    //here we move and reset the snowflakes
    snowSpeed = map(seasonalBoost, 1, 2, 2, 1);

    for (let i = 0; i < snow.length; i++) {
      snow[i].show();
      snow[i].update(snowSpeed, snowSpeed);
    }

    // -------------------------------------
    //            Main mechanics
    // -------------------------------------

    //this calculates the boost to herrings-caught from each of the penguins that are currently Fishing and ice gained from each of the penguins mining.
    let herringBoost = 0;
    let iceBoost = 0;
    for (let each of penguins) {
      if (each.occupation == 'fishing') {
        herringBoost += each.fishing * 0.4;
      }
      if (each.occupation == 'mining') {
        iceBoost += each.mining * 0.4;
      }
    }

    //this calculates the number of herrings added per frame
    herring += ((1 + herringBoost) * map(seasonalBoost, 1, 2, 0.4, 1.5) - penguins.length * 0.65 - eggs.length * 0.25) / 60;

    //this calculates ice added per frame
    ice += iceBoost / 70;

    //this calculates the amount of room for penguins
    roomForPenguins = penguinCapacity - (penguins.length + numberOfEggs);


    //this calculates the boost to egg-survial from each penguin parenting
    parentingBoost = 0;
    for (let each of penguins) {
      if (each.occupation == 'parenting') {
        parentingBoost += each.parenting;
      }
    }

    //this calculates the survival chance of the eggs
    survivalChance = random(0.35) + map(seasonalBoost, 1, 2, -0.2, 0.15) + (parentingBoost * 0.04);

    //this tracks if the eggs hatch and decides if they survive
    for (let i = 0; i < eggs.length; i++) {
      eggs[i].update(survivalChance);
      if (eggs[i].hatched) {
        eggs.splice(i, 1);
        i -= 1;
      }

      // this sets the number of eggs for later use.
      if (eggs.length > 0) {
        numberOfEggs = eggs.length;
      } else {
        numberOfEggs = 0;
      }
    }

    //this keeps track of the hp of the penguins and kills them if they have no more hp and ends the game if all penguins are dead
    for (let i = 0; i < penguins.length; i++) {
      penguins[i].updateHP();
      if (penguins[i].hp < 0) {
        penguins.splice(i, 1);
      }
    }

    // -------------------------------------
    //        Text/interface Display
    // -------------------------------------

    //this draws the line next to penguins and eggs
    push();
    strokeWeight(2);
    stroke(0, 0, 50)
    line(colmn2 - 10, topMargin, colmn2 - 10, topMargin + 130);
    pop();

    //this writes the current season to the screen
    push();
    textStyle(ITALIC);
    textSize(16);
    text(season + " – Year " + year, colmn1, 24)
    pop();

    // here we draw the reasource counters
    push();
    textSize(14);
    textAlign(LEFT, TOP);
    text("Herring: " + floor(herring), colmn1, topMargin)

    if (ice > 0) {
      text("Ice: " + floor(ice), colmn1, topMargin + 20)
    }


    //here we draw the number of penguns and aggs to the screen
    text("Penguins: " + penguins.length + " / " + penguinCapacity, colmn2, topMargin);


    //this displays the egg counter and the price of buying an egg
    text("Eggs: " + numberOfEggs, colmn2, topMargin + 76);
    if (eggsHaveBeenLaid && eggs.length) {
      push();
      textSize(12);
      fill(0, 0, 50);
      text("Price: He. " + floor(eggs[eggs.length - 1].price), colmn2, topMargin + 96);
      pop();
    } else if (eggsHaveBeenLaid) {
      push();
      textSize(12);
      fill(0, 0, 50);
      text("Price: He. " + floor(eggBasePrice), colmn2, topMargin + 96);
      pop();
    } else {
      push();
      textSize(12);
      fill(0, 0, 50);
      text("Price: He. " + floor(eggBasePrice), colmn2, topMargin + 96);
      pop();
    }
    //this shows or hides the egg button
    if (roomForPenguins > 0) {
      eggButton.show();
    } else {
      eggButton.hide();
      push();
      textSize(12);
      fill(0, 0, 50);
      text("You need more \nroom to lay eggs.", colmn2 + 75, topMargin + 70)
      pop();
    }

    //this displays the current penguins name, occupation and stat-bars.
    chosenPenguin.show();

    //this draws the rectangle around the buildings
    push();
    strokeWeight(2);
    stroke(0, 0, 50)
    noFill()
    rect(colmn1 - 10, 200, w - colmn1 - 10, 200);
    pop();

    //this writes the number of igloos and the current price of buying an igloo
    text("Basic Igloos: " + basicIgloos.length, colmn1, boxMargin)
    push();
    textSize(12);
    fill(0, 0, 50);
    text("Price: He. " + floor(basicIglooPrice[0]) + " – Ic. " + floor(basicIglooPrice[1]), colmn1, boxMargin + 20);
    pop();

    pop();

    //if all the penguins are dead, it's game over.
    if (penguins.length == 0) {
      hideDOM();
      manage.showScene(gameOver);
    }

  } //draw ends here

  function hideDOM() {
    let allDOM = selectAll('div')
    let buttons = selectAll('button')
    let selectore = selectAll('select')
    selectore[0].position(-50, -50)
    all = concat(allDOM, buttons);

    for (let i = 0; i < all.length; i++) {
      all[i].remove();
    }
  }

}

function gameOver() {
  this.draw = function() {
    background(0, 0, 94);

    push();
    fill(0, 0, 20);
    textAlign(CENTER);
    textSize(20);
    text("Your penguins are all dead.", width / 2, height / 2);
    pop();

  }
}