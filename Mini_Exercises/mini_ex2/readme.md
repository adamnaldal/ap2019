## Mini exercise 2
![Show your emotions](Screenshot.gif)

[link](https://gl.githack.com/adamnaldal/ap2019/raw/master/Mini_Exercises/mini_ex2/index.html)

### Description
The program consists of two sliders, a button and an emoji made up of geometric shapes. I used both simple shapes like ellipses and rectangles, and slightly more elaborate shapes like the curveVertex() and beginShape() / endShape() functions. I also used quite a lot of variables and learned something about structuring my code so it can be read even thought it is quite long.

### Putting it into perspective
When i was reading the assigned reading it struck me as interesting how emojis are essentially a numeric index and so not very flexible. Yet despite this, the unicode consortium tried to make emojis more customisable. This was broadly a failure, possibly because you can't put humans emotions and experiences into numbers like that.
  Therefore i decided to play with the idea of make an ironic "fully customisable emoji" that could display 10.000 unique emotions. I made the numbers very in your face to show the limitations of the system and played with the idea of having a button that says "show emotions" to show the user the exact position or length of a given element in the face, thereby equating the numbers to the feelings the emoji displays.
