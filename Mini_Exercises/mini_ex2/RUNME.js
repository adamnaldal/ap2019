//creates a template for the sliders
class moodSlider {
  constructor(emote_,x_,y_){
  this.x = x_;
  this.y = y_;
  this.emote = emote_;
  this.slider = createSlider(0,100,50);
  }
  show(){
    this.slider.position(this.x, this.y);
    this.slider.style('width', '80px');
    textSize(16);
    text(this.emote + ": " + this.slider.value(), this.x, this.y-10);
  }
}

//defines the variable that controlls the visibility of the meassurments
var visible = false;

function setup(){
  createCanvas(600,500);
  //creates the two sliders and names them
  joySlide = new moodSlider("Sadness/Joy",10,40);
  surpriseSlide = new moodSlider("Boredom/Surprise",10,100);
}

function draw(){
  push();
  background(240,225,0);
  //draws joy and sadnes slider
  joySlide.show();
  var joy = joySlide.slider.value();

  //draws Surprise and boredom slider
  surpriseSlide.show();
  var surprise = surpriseSlide.slider.value();

  //show/hide emotions button
  //box
  push();
  fill(10,160,255);
  strokeWeight(2);
  stroke(60);
  rect(12,140,80,40,10);
  pop();
  //text
  push();
  textSize(14);
  fill(245);
  textAlign(CENTER, CENTER);
  if (visible == false){
    text("Show \nemotions",14,131,80,40);
  } else{
    text("Hide \nemotions",14,131,80,40);
  }
  pop();

  //-----E-Y-E-B-R-O-W-S-----
  var browY = map(surprise, 0, 100, 0, -30)
  var browR = map(surprise, 0, 100, 0, 14)

  //Draws the LEFT EYEBROW
  push();
    noFill();
    strokeWeight(5);
    angleMode(DEGREES);
    translate(240, 158);
    rotate(browR*(-1));

    beginShape();
    //left eyebrow left end
    curveVertex(210-240, 160-158 + browY);
    curveVertex(210-240, 160-158 + browY);
    //left eyebrow middle
    curveVertex(235-240, 155-158 + browY);
    curveVertex(260-240, 155-158 + browY);
    //left eyebrow right end
    curveVertex(285-240, 160-158 + browY);
    curveVertex(285-240, 160-158 + browY);
    endShape();
  pop();

  //Draws the RIGHT EYEBROW
  push();
    noFill();
    strokeWeight(5);
    translate(360, 158);
    rotate(browR);

    beginShape();
    //right eyebrow left end
    curveVertex(315-360, 160-158 + browY);
    curveVertex(315-360, 160-158 + browY);
    //right eyebrow middle
    curveVertex(340-360, 155-158 + browY);
    curveVertex(365-360, 155-158 + browY);
    //right eyebrow right end
    curveVertex(390-360, 160-158 + browY);
    curveVertex(390-360, 160-158 + browY);
    endShape();

  pop();

  //-----E-Y-E-S-----
  var eyeW = map(joy, 0, 100, 45, 55);
  var eyeH = map(surprise, 0, 100, 5, 50);
  //draws the eyes
  push();
    noFill();
    strokeWeight(5);
    ellipse(250,195,eyeW,eyeH);
    ellipse(350,195,eyeW,eyeH);
  pop();

  //-----M-O-U-T-H-----
  var cornerY = map(joy, 0, 100, 25, -25);
  var cornerX = map(joy, 0, 100, 6, -6);

  push();
    noFill();
    strokeWeight(5);
    translate(0, -10);

    //Draws the CURVED LIP
    beginShape();
    //left corner of mouth
    curveVertex(250 + cornerX, 300 + cornerY);
    curveVertex(250 + cornerX, 300 + cornerY);
    //middle
    curveVertex(255 + (cornerX/1.5), 300 + (cornerY/2));

    curveVertex(280, 300);
    curveVertex(320, 300);

    curveVertex(345 + (cornerX/1.5*(-1)), 300 + (cornerY/2));
    //right corner of mouth
    curveVertex(350 + (cornerX*(-1)), 300 + cornerY);
    curveVertex(350 + (cornerX*(-1)), 300 + cornerY);
    endShape();

    //Draws the FLAT LIP
    beginShape();
    //left corner of mouth
    curveVertex(250 + cornerX, 300 + cornerY);
    curveVertex(250 + cornerX, 300 + cornerY);
    //middle
    curveVertex(280,300 + cornerY);
    curveVertex(320,300 + cornerY);
    //right corner of mouth
    curveVertex(350 + (cornerX*(-1)), 300 + cornerY);
    curveVertex(350 + (cornerX*(-1)), 300 + cornerY);
    endShape();
  pop();
  pop();



  //_________________________________
  //_________MEASSUREMENTS___________
  //_________________________________

  //ties the visible variable to the opacity of the meassurements
  var opacity = 0;

  if (visible == true){
    opacity = 255;
  }else {
    opacity = 0;
  }

  //this push encloses everything that is controlled by the hide/show emotions button
  push();
  fill(0,opacity);


  //-----E-Y-E-B-R-O-W-----
  var leftBrowL = {
    x : 210 + map(browR,0,14,4,0),
    y : 160 + map(browR,0,14,22,0),
  };
  var leftBrowR = {
    x : 276 + map(browR,0,14,8,0),
    y : 120 + map(browR,0,14,42,0),
  };

  //Draws the left brows left coordinates
  push();
    translate(-45,-20);
    textSize(10);
    textFont('Courier');

    text ("x: " + floor(leftBrowL.x) + "\ny: " + floor(leftBrowL.y), floor(leftBrowL.x), floor(leftBrowL.y));
  pop();

  //Draws the left brows right coordinates
  push();
    translate(-25,-24);
    textSize(10);
    textFont('Courier');

    text ("x: " + floor(leftBrowR.x) + "\ny: " + floor(leftBrowR.y), floor(leftBrowR.x), floor(leftBrowR.y));
  pop();

  var rightBrowL = {
    x : 315 + map(browR*(-1),0,14,8,0),
    y : 120 + map(browR,0,14,42,0),
  };
  var rightBrowR = {
    x : 390 + map(browR*(-1),0,14,4,0),
    y : 160 + map(browR,0,14,22,0),
  };

  //Draws the right brows left coordinates
  push();
    translate(-17,-25);
    textSize(10);
    textFont('Courier');

    text ("x: " + floor(rightBrowL.x) + "\ny: " + floor(rightBrowL.y), floor(rightBrowL.x), floor(rightBrowL.y));
  pop();

  //Draws the right brows right coordinates
  push();
    translate(5,-20);
    textSize(10);
    textFont('Courier');

    text ("x: " + floor(rightBrowR.x) + "\ny: " + floor(rightBrowR.y), floor(rightBrowR.x), floor(rightBrowR.y));
  pop();


  //-----E-Y-E-S-----
  push();
  //left eye meassurment
  //text
  textSize(10);
  textFont('Courier');
  //height
  text(floor(eyeH),250-25-(eyeW/2), 195+3);
  //width
  text(floor(eyeW),250-5, 195+20+(eyeH/2));

  //rulers
  push();
    stroke(0,opacity);
    //vertical line
    beginShape();
    //top
    vertex(250-10-(eyeW/2), 195-(eyeH/2));
    vertex(250-7-(eyeW/2), 195-(eyeH/2));
    vertex(250-13-(eyeW/2), 195-(eyeH/2));
    vertex(250-10-(eyeW/2), 195-(eyeH/2));
    //bottom
    vertex(250-10-(eyeW/2), 195+(eyeH/2));
    vertex(250-7-(eyeW/2), 195+(eyeH/2));
    vertex(250-13-(eyeW/2), 195+(eyeH/2));
    vertex(250-10-(eyeW/2), 195+(eyeH/2));
    endShape();

    //horizontal line
    beginShape();
    //left
    vertex(250-(eyeW/2), 195+10+(eyeH/2));
    vertex(250-(eyeW/2), 195+7+(eyeH/2));
    vertex(250-(eyeW/2), 195+13+(eyeH/2));
    vertex(250-(eyeW/2), 195+10+(eyeH/2));
    //right
    vertex(250+(eyeW/2), 195+10+(eyeH/2));
    vertex(250+(eyeW/2), 195+7+(eyeH/2));
    vertex(250+(eyeW/2), 195+13+(eyeH/2));
    vertex(250+(eyeW/2), 195+10+(eyeH/2));
    endShape();
  pop();

  //right eye meassurment
  //text
  textSize(10);
  //height
  text(floor(eyeH),350+15+(eyeW/2), 195+3);
  //width
  text(floor(eyeW),350-5, 195+20+(eyeH/2));

  //ruler
  push();
    stroke(0,opacity);
    //vertical line
    beginShape();
    //top
    vertex(350+10+(eyeW/2), 195-(eyeH/2));
    vertex(350+7+(eyeW/2), 195-(eyeH/2));
    vertex(350+13+(eyeW/2), 195-(eyeH/2));
    vertex(350+10+(eyeW/2), 195-(eyeH/2));
    //bottom
    vertex(350+10+(eyeW/2), 195+(eyeH/2));
    vertex(350+7+(eyeW/2), 195+(eyeH/2));
    vertex(350+13+(eyeW/2), 195+(eyeH/2));
    vertex(350+10+(eyeW/2), 195+(eyeH/2));
    endShape();

    //horizontal line
    beginShape();
    //left
    vertex(350-(eyeW/2), 195+10+(eyeH/2));
    vertex(350-(eyeW/2), 195+7+(eyeH/2));
    vertex(350-(eyeW/2), 195+13+(eyeH/2));
    vertex(350-(eyeW/2), 195+10+(eyeH/2));
    //right
    vertex(350+(eyeW/2), 195+10+(eyeH/2));
    vertex(350+(eyeW/2), 195+7+(eyeH/2));
    vertex(350+(eyeW/2), 195+13+(eyeH/2));
    vertex(350+(eyeW/2), 195+10+(eyeH/2));
    endShape();
  pop();

  //-----M-O-U-T-H-----
  var mouthLineV = constrain(cornerY, 0, 25);
  var mouthTextX;
  if ((350 - cornerX) - (250 + cornerX)<100){
    mouthTextX = 300 - 5;
  } else {
    mouthTextX = 300 - 10
  }
  var mouthOpen;
  if (cornerY<0){
    mouthOpen = cornerY*(-1);
  } else{
    mouthOpen = cornerY;
  }

  //text
  //width
  text(floor((350 - cornerX) - (250 + cornerX)), mouthTextX, 300 + mouthLineV + 10);
  //height
  text(floor(mouthOpen),216 + cornerX, 290 + 3 + cornerY/2);

  //rulers
  push();
    stroke(0,opacity);
    //horizontal line
    beginShape();
    //left
    vertex(250 + cornerX, 300 + mouthLineV);
    vertex(250 + cornerX, 300 - 3 + mouthLineV);
    vertex(250 + cornerX, 300 + 3 + mouthLineV);
    vertex(250 + cornerX, 300 + mouthLineV);
    //right
    vertex(350 - cornerX, 300 + mouthLineV);
    vertex(350 - cornerX, 300 - 3 + mouthLineV);
    vertex(350 - cornerX, 300 + 3 + mouthLineV);
    vertex(350 - cornerX, 300 + mouthLineV);
    endShape();

    //vertical line
    beginShape();
    //top
    vertex(250-10+cornerX, 290 + cornerY);
    vertex(250-13+cornerX, 290 + cornerY);
    vertex(250-7+cornerX, 290 + cornerY);
    vertex(250-10+cornerX, 290 + cornerY);
    //bottom
    vertex(250-10+cornerX, 290);
    vertex(250-13+cornerX, 290);
    vertex(250-7+cornerX, 290);
    vertex(250-10+cornerX, 290);
    endShape();
  pop();
  pop();
  pop();
}

//changes the state of visible when the show/hide emotions button is pressed
function mouseClicked(){
  console.log(mouseX + ", " + mouseY);
  if (mouseX<90 && mouseX>12 && mouseY<180 && mouseY>140){
    visible = !visible;
  }
}
