# Eksamen – talepapir
## Materiality of interaction
* Valgt Wiberg’s materiality of interaction begreb
* herunder:
	* interaction first princippet
	* 6 interaktionsdimensioner
		- [ ] change of state
		- [ ] speed of change
		- [ ] request for input
		- [ ] responsiveness to input
		- [ ] single-threaded og multithreaded
		- [ ] direct og agent based

## Agenda

## Hi-Cache
### Wiberg
* mål med app:
	* motivere til at komme ud i naturen med en social gulerod
	* fokus på omgivelser ikke telefon
* Mobil app:
	* forudbestemt fysisk materialitet
	* ens lokation bliver til en interaktionsform gennem gps - immaterielt materiale 
* 6 interaktionsdimensioner
	* singlethreaded under gåturen for at fremme opmærksomhed på omgivelserne


## Soundwave
* vi skulle arbejde med tangible interaktion og lave en “musik afspiller”
	* Hvordan virker designet
### Wiberg
* styrer lyd i rummet gennem interaktion med vand
	* interaction first:
		* vandet er valgt fordi interaktionsmodaliteten bliver meget tæt forbundet til musikken.
	* analog change of state
	* høj responsiveness og speed of change
	* material turn -> bevæger sig væk fra symbolsk repræsentation
		* i stedet benyttes en materiel metafor hvor manipulationen er direkte stedet for medieret gennem repræsentation 


## Logbogen
* skulle lave noget inden for ubiquitous interfaces
* væktøj til diskret bogførelse af smartabuse
	* hvordan virker det
### Wiberg
* sensorer i huset kommunikerer trådløst med en nøgle som har knap og logbog.
* agent based interaction i høj grad 
	* heller ingen request for input eller turn taking
* hovedmålet her var sikkerhed gennem diskretion fremfor lækker interaktion..
	* det kan ses i hvor designet ligger på interaktionsdimensionerne


## Oslagstavlen
* multiplatformsprogram til selvorganiseret aktivitet mellem seniorer
### Wiberg
* skeumorfisme: ikke særligt wibergsk.
	* begrundet valg på baggrund af research 
		* computer anxiety
		* interaktionen er faciliterende for menneskeligt sammenværd - vi vil gerne have så mange ældre med som muligt
* change of state
	* tydeliggjort så man nemt kan følge med i hvor man er og hvad man har af muligheder.


#uni/3/Interaktionsteknologier