//For this sketch i use the speech syntesis and speech recognition library by R. Luke DuBois (dubois@nyu.edu)  for p5.js.

let w;
let h;

let soliloquy;
let robot;
let playbook;
let fiona;
let charles;
let currentSpeaker;
let sfx;

let position = 0;

function preload() {
  playbook = loadJSON('conversation.JSON')
  robot = new p5.Speech('Fiona');
  sfx = loadSound('phone_dial_sound.mp3')
  //robot.onLoad = converse;
}

function setup() {
  w = windowWidth;
  h = windowHeight;
  createCanvas(w, h);
  colorMode(HSB, 360, 100, 100, 100);
  frameRate(30);
  angleMode(DEGREES);

  console.log("nr. of sentences: " + playbook.list.length)
  robot.onEnd = finishSpeaking;

  fiona = new fionaAnimation();
  charles = new charlesAnimation();
  //converse();
}

function draw() {
  background(210, 0, 100);

  fiona.show();
  charles.show();

}

class fionaAnimation{
  constructor(){

    this.xOrigin = w*0.3;
    this.yOrigin = h/2;
    this.xOfset = 15;
    this.yOfset = 0;
    this.movement = 0;
    this.diaOfset = map(noise(this.movement/20),0,1,-10,10);
    this.dia = 30;
  }

  show(){
    push();
    noStroke();
    fill(280, 50, 90);
    circle(this.xOrigin + this.xOfset, this.yOrigin + this.yOfset, this.dia + this.diaOfset);
    pop();

    if (currentSpeaker == "Fiona"){

      this.diaOfset = map(noise(this.movement/20),0,1,-10,10);
      this.yOfset = sin(this.movement*3) * 15;
      this.xOfset = cos(this.movement*3 ) * 15;
      this.movement ++;

    }
  }
}

class charlesAnimation{
  constructor(){
    this.dia = 80;
    this.x = w*0.7;
    this.y = h/2;
    this.origin = 0;
    this.end = 0;
    this.movement=0;
    this.ballDia = map((this.end + 180)-this.origin,0,360, 5, 30);
  }

  show(){
    push();
    strokeWeight(5);
    stroke(160, 50, 90);
    arc(this.x, this.y, this.dia, this.dia, this.origin, 180 + this.end)
    pop();
    push();
    noStroke();
    fill(160,50,90);
    circle(this.x, this.y, this.ballDia);
    pop();

    if (currentSpeaker == "Charles"){

      this.origin = (sin(this.movement/2 *3)) * 90;
      this.end = sin(this.movement*3) * 90;
      this.ballDia = map((this.end + 180)-this.origin,0, 360, 5, 30);
      this.movement ++;

    }
  }
}

function windowResized() {
  w = windowWidth;
  h = windowHeight;
  fiona.xOrigin = w*0.3;
  fiona.yOrigin = h/2;
  charles.x = w*0.7;
  charles.y = h/2;
  resizeCanvas(w, h);
  resizeCanvas(windowWidth, windowHeight);
}

function converse(){
  setTimeout(function () {
        console.log(playbook.list[position].speaker + " is speaking. " + "waited " + playbook.list[position].pause + " millis");
        robot.setVoice(playbook.list[position].speaker);
        robot.speak(playbook.list[position].line);
        currentSpeaker = playbook.list[position].speaker;
     }, playbook.list[position].pause)
}

function finishSpeaking(){
  console.log("i'm done")
  currentSpeaker = "none"

  position++;
  if (position < playbook.list.length) {
     converse();
  }

  if (position == 1){
    sfx.play();
    setTimeout(function(){
      sfx.stop();
    },playbook.list[position].pause - 200)
  }
}
